package in.net.webinfotech.santirekha.presentation.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.util.GlideHelper;

/**
 * Created by Raj on 05-01-2019.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    Context mContext;
    Callback mCallback;
    Product[] productLists;

    public interface Callback{
        void onClickOfImage(int id);
        void onClickOfCart(int productId, int quantity);
    }

    public ProductListAdapter(Context context, Callback callback, Product[] productLists){
        this.mContext = context;
        this.mCallback = callback;
        this.productLists = productLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycleview_productlist, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.productPoster, mContext.getResources().getString(R.string.base_url) + "uploads/product_image/thumb/" + productLists[position].image);
        holder.productName.setText(productLists[position].name);
        holder.productPrice.setText("Rs " + productLists[position].price);
        if(productLists[position].mrp > 0){
            holder.txtViewProductSellingPrice.setText("MRP " + productLists[position].mrp);
        }else{
            holder.txtViewProductSellingPrice.setText("");
        }
        holder.txtViewProductSellingPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.productPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onClickOfImage(productLists[position].id);
            }
        });
        if(productLists[position].stock == 0){
            holder.btnCart.setEnabled(false);
            holder.btnCart.setText("Out of Stock");
        }
        holder.btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(productLists[position].stock == 0){
                    Toast.makeText(mContext.getApplicationContext(), "Out of Stock", Toast.LENGTH_SHORT).show();
                }else {
                    mCallback.onClickOfCart(productLists[position].id, 1);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return productLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        @BindView(R.id.imageViewProduct)
        ImageView productPoster;
        @BindView(R.id.txtProductName)
        TextView productName;
        @BindView(R.id.txtProductPrice)
        TextView productPrice;
        @BindView(R.id.btnCart)
        Button btnCart;
        @BindView(R.id.txtProductSellingPrice)
        TextView txtViewProductSellingPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = itemView;
        }
    }

    public void addItems(Product[] newProducts){
        this.productLists = newProducts;
    }

}
