package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.Orders.OrderHistory;

/**
 * Created by Raj on 22-02-2019.
 */

public interface GetOrderHistoryInteractor {
    interface Callback{
        void onGettingOrderHistorySuccess(OrderHistory[] orderHistories);
        void onGettingOrderHistoryFail(String errorMsg);
    }
}
