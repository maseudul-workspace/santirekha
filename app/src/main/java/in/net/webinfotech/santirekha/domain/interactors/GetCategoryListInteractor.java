package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.Category.Category;

/**
 * Created by Raj on 11-02-2019.
 */

public interface GetCategoryListInteractor {
    interface Callback{
        void onGettingCategoryListSuccess(Category[] categories);
        void onGettingCategoryListFail(String errorMsg);
    }
}
