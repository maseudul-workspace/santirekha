package in.net.webinfotech.santirekha.domain.model.Category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 11-02-2019.
 */

public class Category {
    @SerializedName("id")
    @Expose
    public int Id;

    @SerializedName("category")
    @Expose
    public String category;

    @SerializedName("subcategory")
    @Expose
    public SubCategory[] subCategories;

}
