package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.User.UserDetails;

/**
 * Created by Raj on 13-02-2019.
 */

public interface GetUserDetailsInteractor {
    interface Callback{
        void onGettingUserDetailsSuccess(UserDetails userDetails);
        void onGettingUserDetailsFail(String errorMsg);
    }
}
