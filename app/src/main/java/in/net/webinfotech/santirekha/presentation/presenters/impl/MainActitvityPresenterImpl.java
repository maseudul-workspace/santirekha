package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.CheckUserInteractor;
import in.net.webinfotech.santirekha.domain.interactors.FetchReviewsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetCartCountInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetCategoryListInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetImageSlidersInteractor;
import in.net.webinfotech.santirekha.domain.interactors.SubmitReviewInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.CheckUserInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.FetchReviewsInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetCartCountInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetCategoryListInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetImageSlidersInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.SubmitReviewInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Category.Category;
import in.net.webinfotech.santirekha.domain.model.ImageSliders.ImageSlider;
import in.net.webinfotech.santirekha.domain.model.Review.Review;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.domain.model.User.UserStatusCheckResponse;
import in.net.webinfotech.santirekha.presentation.presenters.MainActivityPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.routers.MainRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ReviewsAdapter;
import in.net.webinfotech.santirekha.repository.Category.CategoryRepositoryImpl;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 10-01-2019.
 */

public class MainActitvityPresenterImpl extends AbstractPresenter implements MainActivityPresenter,
                                                                                GetCartCountInteractor.Callback,
                                                                                GetCategoryListInteractor.Callback,
                                                                                SubmitReviewInteractor.Callback,
                                                                                FetchReviewsInteractor.Callback,
                                                                                CheckUserInteractor.Callback,
                                                                                GetImageSlidersInteractor.Callback
                                                                                {

    Context mContext;
    GetCartCountInteractorImpl mIntearctor;
    int userId;
    MainActivityPresenter.View mView;
    AndroidApplication androidApplication;
    GetCategoryListInteractorImpl categoryListInteractor;
    SubmitReviewInteractorImpl submitReviewInteractor;
    ReviewsAdapter adapter;
    FetchReviewsInteractorImpl fetchReviewsInteractor;
    CheckUserInteractorImpl userInteractor;
    MainRouter mRouter;
    GetImageSlidersInteractorImpl imageSlidersInteractor;

    public MainActitvityPresenterImpl(Executor executor,
                                      MainThread mainThread,
                                      Context context,
                                      MainActivityPresenter.View view,
                                      MainRouter router
                                      ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void getCategoriesList() {
        categoryListInteractor = new GetCategoryListInteractorImpl(mExecutor, mMainThread, this, new CategoryRepositoryImpl());
        categoryListInteractor.execute();
    }

    @Override
    public void onGettingCategoryListSuccess(Category[] categories) {
        mView.loadCategories(categories);
    }

    @Override
    public void onGettingCategoryListFail(String errorMsg) {

    }

    @Override
    public void getCartCount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null){
            mIntearctor = new GetCartCountInteractorImpl(mExecutor,
                    mMainThread,
                    new CartRepositoryImpl(),
                    this,
                    userInfo.userId, userInfo.apiKey);
            mIntearctor.execute();
        }
    }

    @Override
    public void onGetCartCountSuccess(int count) {
        mView.loadCartCount(count);
    }

    @Override
    public void onGetCartCountFail(String errorMsg) {
        mView.loadCartCount(0);
    }

    @Override
    public void submitReview(int rating, String comments) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        submitReviewInteractor = new SubmitReviewInteractorImpl(mExecutor, mMainThread,
                this, new UserRepositoryImpl(), userInfo.userId, userInfo.apiKey, rating, comments);
        submitReviewInteractor.execute();
    }

    @Override
    public void onSubmitReviewSuccess(String successMsg) {
        Toast.makeText(mContext, successMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSubmitReviewFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void fetchReviews() {
        fetchReviewsInteractor = new FetchReviewsInteractorImpl(mExecutor, mMainThread, this, new GetProductsRepositoryImpl());
        fetchReviewsInteractor.execute();
    }

    @Override
    public void onGettingReviewsSuccess(Review[] reviews) {
        adapter = new ReviewsAdapter(mContext, reviews);
        mView.loadReviews(adapter);
    }

    @Override
    public void checkUserStatus() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if(userInfo != null){
            userInteractor = new CheckUserInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.userId);
            userInteractor.execute();
        }
    }

    @Override
    public void getSliderImages() {
        imageSlidersInteractor = new GetImageSlidersInteractorImpl(mExecutor, mMainThread, this, new CategoryRepositoryImpl());
        imageSlidersInteractor.execute();
    }

    @Override
    public void onCheckingUserFail() {

    }

    @Override
    public void onCheckingUserSuccess(UserStatusCheckResponse userStatusCheckResponse) {
        if(!userStatusCheckResponse.status){
            Toast.makeText(mContext, userStatusCheckResponse.message, Toast.LENGTH_SHORT).show();
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext.getApplicationContext(), null);
            mRouter.goToLogin();
        }
    }

    @Override
    public void onGettingReviewsFail() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onGettingImageSlidersSuccess(ImageSlider[] imageSliders) {
        mView.loadSliderImages(imageSliders);
    }

    @Override
    public void ongettingImageSliderFail(String errorMsg) {

    }
}
