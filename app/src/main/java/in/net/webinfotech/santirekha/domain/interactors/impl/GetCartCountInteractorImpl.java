package in.net.webinfotech.santirekha.domain.interactors.impl;

import android.util.Log;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetCartCountInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Cart.CartListWrapper;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;

/**
 * Created by Raj on 09-01-2019.
 */

public class GetCartCountInteractorImpl extends AbstractInteractor implements GetCartCountInteractor {

    CartRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;

    public GetCartCountInteractorImpl(Executor threadExecutor, MainThread mainThread,
                                      CartRepositoryImpl repository, Callback callback, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGetCartCountFail(errorMsg);
            }
        });
    }

    private void postMessage(final int count){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGetCartCountSuccess(count);
            }
        });
    }



    @Override
    public void run() {
        final CartListWrapper cartListWrapper = mRepository.getCartDetails(userId, apiKey);
        if(cartListWrapper == null){
            notifyError("Something went wrong");
        }else if(!cartListWrapper.status){
            notifyError(cartListWrapper.message);
        }
        else{
            postMessage(cartListWrapper.carts.length);
        }
    }
}
