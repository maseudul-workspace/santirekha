package in.net.webinfotech.santirekha.repository.ProductRepository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Raj on 05-01-2019.
 */

public interface GetProductsRepository {
    @POST("api/product/product_list_cat_wise.php")
    @FormUrlEncoded
    Call<ResponseBody> getProductListByCategory(@Field("type") int type,
                                                @Field("page_no") int pageNo,
                                                @Field("cat_id") int categoryId);

    @POST("api/product/product_details.php")
    @FormUrlEncoded
    Call<ResponseBody> getProductDetailsById(@Field("product_id") int productId);

    @POST("api/product/product_search.php")
    @FormUrlEncoded
    Call<ResponseBody> searchProducts(@Field("search_key") String search_key,
                                  @Field("page_no") int page_no
    );

    @POST("api/review/customer_review_fetch.php")
    Call<ResponseBody> fetchReviews();

}
