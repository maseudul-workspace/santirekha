package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.Category.Category;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
//    ExpandableDrawerItem manCareDrawerItem;
//    ExpandableDrawerItem fastFooDrawerItem;
//    ExpandableDrawerItem groceryDrawerItem;
//    ExpandableDrawerItem womanDrawerItem;
//    ExpandableDrawerItem educationDrawerItem;
//    ExpandableDrawerItem homeCareDrawerItem;
//    ExpandableDrawerItem babyCareDrawerItem;
//    ExpandableDrawerItem fruitsDrawerItem;
//    ExpandableDrawerItem packageDrawerItem;
//    ExpandableDrawerItem pujaDrawerItem;
//    ExpandableDrawerItem beverageDrawerItem;
//    ExpandableDrawerItem healthCareDrawerItem;
    AndroidApplication androidApplication;
    Drawer result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected void inflateContent(@LayoutRes int inflatedResID) {
        setContentView(R.layout.activity_base);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflatedResID, contentFrameLayout);
        ButterKnife.bind(this);
        setUpDrawer();
    }

    public void setUpDrawer(){
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowTitleEnabled(false);
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.santirekhalogo)
                .withHeaderBackgroundScaleType(ImageView.ScaleType.FIT_CENTER)
                .build();
//        manCareDrawerItem = new ExpandableDrawerItem().withName("Men Care").withSelectable(false).withIcon(R.drawable.men_category);
//        womanDrawerItem = new ExpandableDrawerItem().withName("Women Care").withSelectable(false).withIcon(R.drawable.women_category);
//        babyCareDrawerItem = new ExpandableDrawerItem().withName("Baby Care").withSelectable(false).withIcon(R.drawable.baby_category);
//        homeCareDrawerItem = new ExpandableDrawerItem().withName("Home Care").withIcon(R.drawable.home_care_category).withSelectable(false);
//        fastFooDrawerItem = new ExpandableDrawerItem().withName("Fast Food").withSelectable(false).withIcon(R.drawable.fastfood_category);
//        educationDrawerItem = new ExpandableDrawerItem().withName("Education").withSelectable(false).withIcon(R.drawable.education_category);
//        groceryDrawerItem = new ExpandableDrawerItem().withName("Grocery").withSelectable(false).withIcon(R.drawable.grocery_category);
//        fruitsDrawerItem =  new ExpandableDrawerItem().withName("Fruits").withSelectable(false).withIcon(R.drawable.fruits_category);
////        packageDrawerItem = new ExpandableDrawerItem().withName("Package").withSelectable(false).withIcon(R.drawable.package_category);
//        pujaDrawerItem = new ExpandableDrawerItem().withName("Puja").withSelectable(false).withIcon(R.drawable.puja_category);
//        beverageDrawerItem = new ExpandableDrawerItem().withName("Beverage").withSelectable(false).withIcon(R.drawable.beverage_category);
//        healthCareDrawerItem = new ExpandableDrawerItem().withName("Health Care").withSelectable(false).withIcon(R.drawable.health_category);

        result = new DrawerBuilder()
        .withActivity(this)
        .withAccountHeader(headerResult)
        .withToolbar(toolbar)
        .withHasStableIds(true)
        .withDisplayBelowStatusBar(false)
        .withActionBarDrawerToggleAnimated(true)
        .addDrawerItems(
                new PrimaryDrawerItem().withName("Home").withIcon(R.drawable.home_menu).withSelectable(false).withIdentifier(500),
//                new PrimaryDrawerItem().withName("Package").withIcon(R.drawable.package_category).withSelectable(false).withIdentifier(1009),
//                manCareDrawerItem,
//                womanDrawerItem,
//                babyCareDrawerItem,
//                homeCareDrawerItem,
//                fastFooDrawerItem,
//                educationDrawerItem,
//                groceryDrawerItem,
//                fruitsDrawerItem,
//                packageDrawerItem,
//                pujaDrawerItem,
//                beverageDrawerItem,
//                healthCareDrawerItem,
                new PrimaryDrawerItem().withName("About Us").withIcon(R.drawable.about_menu).withSelectable(false).withIdentifier(501),
                new PrimaryDrawerItem().withName("Order Schedule").withIcon(R.drawable.order_menu).withSelectable(false).withIdentifier(516),
                new PrimaryDrawerItem().withName("Delivery Schedule").withIcon(R.drawable.shiiping_address).withSelectable(false).withIdentifier(517),
                new PrimaryDrawerItem().withName("Off Line").withIcon(R.drawable.offline).withSelectable(false).withIdentifier(518),
                new PrimaryDrawerItem().withName("Contact Us").withIcon(R.drawable.phone_black).withSelectable(false).withIdentifier(519)
                )
        .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                // do something with the clicked item :D
                if(drawerItem != null){
                    if((int) drawerItem.getIdentifier() >= 500 && (int) drawerItem.getIdentifier() <= 2000) {
                        switch (((int) drawerItem.getIdentifier())) {
                            case 501:
                                Intent aboutIntent = new Intent(getApplicationContext(), AboutActivity.class);
                                startActivity(aboutIntent);
                                break;
                            case 505:
                                Intent logInIntent = new Intent(getApplicationContext(), LogInActivity.class);
                                startActivity(logInIntent);
                                break;
                            case 506:
                                androidApplication = (AndroidApplication) getApplicationContext();
                                androidApplication.setUserInfo(getApplicationContext(), null);
                                result.addItem(new PrimaryDrawerItem().withName("Log In").withIcon(R.drawable.login_icon).withIdentifier(505).withSelectable(false));
                                result.removeItem(506);
                                result.removeItem(507);
                                result.removeItem(508);
                                result.removeItem(509);
                                result.removeItem(510);
                                result.removeItem(511);
                                result.removeItem(512);
                                Toast.makeText(getApplicationContext(), "Successfully logged out",Toast.LENGTH_SHORT).show();
                                break;
                            case 508:
                                Intent userDetailIntent = new Intent(getApplicationContext(), UserDetailsActivity.class);
                                startActivity(userDetailIntent);
                                break;
                            case 509:
                                Intent changePasswordIntent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(changePasswordIntent);
                                break;
                            case 510:
                                Intent shippingAddressIntent = new Intent(getApplicationContext(), ShippingAddressActivity.class);
                                startActivity(shippingAddressIntent);
                                break;
                            case 511:
                                Intent ordersIntent = new Intent(getApplicationContext(), OrderHistoryActivity.class);
                                startActivity(ordersIntent);
                                break;
                            case 512:
                                Intent downlineIntent = new Intent(getApplicationContext(), DownlineActivity.class);
                                startActivity(downlineIntent);
                                break;
                            case 1010:
                                Intent productListIntent = new Intent(getApplicationContext(), ProductListActivity.class);
                                productListIntent.putExtra("type", 1);
                                productListIntent.putExtra("categoryId", 1010);
                                startActivity(productListIntent);
                                break;
                            case 1009:
                                Intent packageListIntent = new Intent(getApplicationContext(), PackageListActivity.class);
                                packageListIntent.putExtra("type", 1);
                                packageListIntent.putExtra("categoryId", 1009);
                                startActivity(packageListIntent);
                                break;
                            case 1012:
                                Intent productListIntent2 = new Intent(getApplicationContext(), ProductListActivity.class);
                                productListIntent2.putExtra("type", 1);
                                productListIntent2.putExtra("categoryId", 1012);
                                startActivity(productListIntent2);
                                break;
                            case 519:
                                Intent contactUsIntent = new Intent(getApplicationContext(), ContactActivity.class);
                                startActivity(contactUsIntent);
                                break;
                            case 517:
                                Intent deliveryScheduleIntent = new Intent(getApplicationContext(), DeliveryScheduleActivity.class);
                                startActivity(deliveryScheduleIntent);
                                break;
                            case 516:
                                Intent orderScheduleIntent = new Intent(getApplicationContext(), OrderScheduleActivity.class);
                                startActivity(orderScheduleIntent);
                                break;
                            case 518:
                                Intent offLineIntent = new Intent(getApplicationContext(), OffLineActivity.class);
                                startActivity(offLineIntent);
                                break;
                        }
                    }else if((int) drawerItem.getIdentifier() >= 1 && (int) drawerItem.getIdentifier() <= 200){

                        Intent productListIntent = new Intent(getApplicationContext(), ProductListActivity.class);
                        productListIntent.putExtra("type", 2);
                        productListIntent.putExtra("categoryId", (int) drawerItem.getIdentifier());
                        startActivity(productListIntent);
                    }
                }
                return false;
            }
        })
        .build();
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            result.addItem(new PrimaryDrawerItem().withName("Log In").withIcon(R.drawable.login_icon).withIdentifier(505).withSelectable(false));
        }else{
            result.addItem(new ExpandableDrawerItem().withName("My Account").withIdentifier(507).withIcon(R.drawable.username).withSelectable(false)
                    .withSubItems(
                            new SecondaryDrawerItem().withName("My Profile").withIcon(R.drawable.username).withLevel(2).withIdentifier(508),
                            new SecondaryDrawerItem().withName("Change password").withIcon(R.drawable.password).withLevel(2).withIdentifier(509),
                            new SecondaryDrawerItem().withName("Shipping address").withIcon(R.drawable.shiiping_address).withLevel(2).withIdentifier(510),
                            new SecondaryDrawerItem().withName("My Orders").withIcon(R.drawable.order_menu).withLevel(2).withIdentifier(511),
                            new SecondaryDrawerItem().withName("My Business").withIcon(R.drawable.downlines).withLevel(2).withIdentifier(512)
                    ));
            result.addItem(new PrimaryDrawerItem().withName("Log Out").withIcon(R.drawable.logout_menu).withIdentifier(506).withSelectable(false));
        }
    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

//    protected void setCategories(Category[] categories){
//        for(int i = 0; i < categories.length; i++){
//            switch (categories[i].Id){
//                case 1001:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                                                        withName(categories[i].subCategories[j].subCatName)
//                                                                        .withIdentifier(categories[i].subCategories[j].subCatId)
//                                                                        .withLevel(2);
//                        manCareDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1002:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        fastFooDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1003:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        groceryDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1004:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        womanDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1005:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        educationDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1006:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        homeCareDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1007:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        babyCareDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1008:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        fruitsDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
////                case 1009:
////                    for(int j = 0; j < categories[i].subCategories.length; j++){
////                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
////                                withName(categories[i].subCategories[j].
////                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
////                        packageDrawerItem.withSubItems(secondaryDrawerItem);
////                    }
////                    break;
//                case 1010:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        pujaDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1011:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        beverageDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//                case 1012:
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        SecondaryDrawerItem secondaryDrawerItem = new SecondaryDrawerItem().
//                                withName(categories[i].subCategories[j].
//                                        subCatName).withIdentifier(categories[i].subCategories[j].subCatId).withLevel(2);
//                        healthCareDrawerItem.withSubItems(secondaryDrawerItem);
//                    }
//                    break;
//
//            }
//        }
//    }

    @OnClick(R.id.img_view_search) void searchClicked(){
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

}
