package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetProductListByCategoryInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.AddToCartInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetProductListByCategoryInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.ProductListPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.routers.ProductListActivityRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ProductListAdapter;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 05-01-2019.
 */

public class ProductListPresenterImpl extends AbstractPresenter implements ProductListPresenter,
                                                                            GetProductListByCategoryInteractor.Callback,
                                                                            ProductListAdapter.Callback,
                                                                            AddToCartInteractor.Callback{

    Context mContext;
    GetProductListByCategoryInteractorImpl mInteractor;
    AddToCartInteractorImpl mCartInteractor;
    ProductListPresenter.View mView;
    ProductListAdapter adapter;
    ProductListActivityRouter mRouter;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    int userId;
    Product[] newProducts;

    public ProductListPresenterImpl(Executor executor,
                                    MainThread mainThread,
                                    Context context,
                                    ProductListPresenter.View view,
                                    ProductListActivityRouter router) {
        super(executor, mainThread);
        mContext = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void onGettingProductListSuccess(Product[] productLists, int totalPage) {
        Product[] tempProducts;
        tempProducts = newProducts;
        try {
            int len1 = tempProducts.length;
            int len2 = productLists.length;
            newProducts = new Product[len1 + len2];
            System.arraycopy(tempProducts, 0, newProducts, 0, len1);
            System.arraycopy(productLists, 0, newProducts, len1, len2);
            adapter.addItems(newProducts);
            adapter.notifyDataSetChanged();
            mView.hidePaginationProgressBar();
        }catch (NullPointerException e){
            newProducts = productLists;
            adapter = new ProductListAdapter(mContext, this, productLists);
            mView.loadProductList(adapter, totalPage);
            mView.hideProgressList();
        }
    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        mView.hideProgressList();
        Toast.makeText(mContext, "No products found for this category", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getProductList(int type, int pageNo, int categoryId, String state) {
        if(state.equals("refresh")){
            newProducts = null;
        }
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        mInteractor = new GetProductListByCategoryInteractorImpl(mExecutor,
                                                                 mMainThread,
                                                                    this,
                                                                    new GetProductsRepositoryImpl(),
                                                                    type, pageNo, categoryId
                                                                    );
        mInteractor.execute();
    }

    @Override
    public void onClickOfImage(int id) {
        this.mRouter.goToProductDetailsActivity(id);
    }

    @Override
    public void onClickOfCart(int productId, int quantity) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext.getApplicationContext());
        if(userInfo == null){
            mView.showLoginSnackbar();
        }else {
            mView.showProgressList();
            String apiKey = userInfo.apiKey;
            int userId = userInfo.userId;
            mCartInteractor = new AddToCartInteractorImpl(mExecutor,
                    mMainThread,
                    new CartRepositoryImpl(),
                    this,
                    apiKey, productId,
                    userId, quantity
            );
            mCartInteractor.execute();
        }
    }

    @Override
    public void onAddToCartSuccess(String successMsg) {
        mView.hideProgressList();
        Toast.makeText(mContext,"Successfully added to cart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.hideProgressList();
        if(errorMsg.equals("GoToCart")){
            Toast.makeText(mContext, "Already added to cart", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
