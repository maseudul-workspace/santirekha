package in.net.webinfotech.santirekha.domain.interactors;

/**
 * Created by Raj on 18-02-2019.
 */

public interface DeleteShippingAddressInteractor {
    interface Callback{
        void onAddressDeleteSucess(String successMsg);
        void onAddressDeleteFail(String errorMsg);
    }
}
