package in.net.webinfotech.santirekha.presentation.routers;

/**
 * Created by Raj on 10-01-2019.
 */

public interface CartIDetailsActivityRouter {
    void goToProductDetailsActivity(int productId);
}
