package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.domain.model.User.UserDetails;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.UserDetailsPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.UserDetailsPresenterImpl;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class UserDetailsActivity extends AppCompatActivity implements UserDetailsPresenter.View{

    UserDetailsPresenterImpl mPresenter;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    @BindView(R.id.txt_view_name)
    TextView txtViewName;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.txt_view_email)
    TextView txtViewEmail;
    @BindView(R.id.txt_view_phone)
    TextView txtViewPhone;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.txt_view_city)
    TextView txtViewCity;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.txt_view_pin)
    TextView textViewPin;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.txt_view_address)
    TextView textViewAddress;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.btn_update)
    Button updateButton;
    @BindView(R.id.txt_view_email_edit)
    TextView txtEmailEdit;
    @BindView(R.id.txt_view_name_edit)
    TextView txtNameEdit;
    @BindView(R.id.txt_view_address_edit)
    TextView txtAddressEdit;
    @BindView(R.id.txt_view_pin_edit)
    TextView txtPinEdit;
    @BindView(R.id.txt_view_city_edit)
    TextView txtCityEdit;
    @BindView(R.id.txt_view_state_edit)
    TextView txtStateEdit;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        initialisePresenter();
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        showProgressBar();
        mPresenter.fetchUserDetails(userInfo.userId, userInfo.apiKey);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.fetchUserDetails(userInfo.userId, userInfo.apiKey);
            }
        });
    }

    public void initialisePresenter(){
        mPresenter = new UserDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadUserDetails(UserDetails userDetails) {
        txtViewName.setText(userDetails.name);
        editTextName.setText(userDetails.name);

        txtViewEmail.setText(userDetails.email);
        editTextEmail.setText(userDetails.email);

        txtViewPhone.setText(Long.toString(userDetails.mobile));

        txtViewState.setText(userDetails.permanentAddress.state);
        editTextState.setText(userDetails.permanentAddress.state);

        txtViewCity.setText(userDetails.permanentAddress.city);
        editTextCity.setText(userDetails.permanentAddress.city);

        textViewAddress.setText(userDetails.permanentAddress.location);
        editTextAddress.setText(userDetails.permanentAddress.location);

        if(Long.toString(userDetails.permanentAddress.pin).equals("0")){
            textViewPin.setText("");
            editTextPin.setText("");
        }else{
            textViewPin.setText(Long.toString(userDetails.permanentAddress.pin));
            editTextPin.setText(Long.toString(userDetails.permanentAddress.pin));
        }
    }

    @OnClick(R.id.txt_view_email_edit) void showEmailEditText(){
        editTextEmail.setVisibility(View.VISIBLE);
        txtViewEmail.setVisibility(View.GONE);
        updateButton.setVisibility(View.VISIBLE);
        txtEmailEdit.setVisibility(View.GONE);
    }

    @OnClick(R.id.txt_view_name_edit) void showNameEditText(){
        editTextName.setVisibility(View.VISIBLE);
        txtViewName.setVisibility(View.GONE);
        updateButton.setVisibility(View.VISIBLE);
        txtNameEdit.setVisibility(View.GONE);
    }

    @OnClick(R.id.txt_view_pin_edit) void showPinEditText(){
        editTextPin.setVisibility(View.VISIBLE);
        textViewPin.setVisibility(View.GONE);
        updateButton.setVisibility(View.VISIBLE);
        txtPinEdit.setVisibility(View.GONE);
    }

    @OnClick(R.id.txt_view_address_edit) void showAddressEditText(){
        editTextAddress.setVisibility(View.VISIBLE);
        textViewAddress.setVisibility(View.GONE);
        updateButton.setVisibility(View.VISIBLE);
        txtAddressEdit.setVisibility(View.GONE);
    }

    @OnClick(R.id.txt_view_city_edit) void showCityEditText(){
        editTextCity.setVisibility(View.VISIBLE);
        txtViewCity.setVisibility(View.GONE);
        updateButton.setVisibility(View.VISIBLE);
        txtCityEdit.setVisibility(View.GONE);
    }

    @OnClick(R.id.txt_view_state_edit) void showStateEditText(){
        editTextState.setVisibility(View.VISIBLE);
        txtViewState.setVisibility(View.GONE);
        updateButton.setVisibility(View.VISIBLE);
        txtStateEdit.setVisibility(View.GONE);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void stopRefreshing() {
        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);

            editTextEmail.setVisibility(View.GONE);
            txtViewEmail.setVisibility(View.VISIBLE);

            editTextName.setVisibility(View.GONE);
            txtViewName.setVisibility(View.VISIBLE);

            editTextPin.setVisibility(View.GONE);
            textViewPin.setVisibility(View.VISIBLE);

            editTextAddress.setVisibility(View.GONE);
            textViewAddress.setVisibility(View.VISIBLE);

            editTextCity.setVisibility(View.GONE);
            txtViewCity.setVisibility(View.VISIBLE);

            editTextState.setVisibility(View.GONE);
            txtViewState.setVisibility(View.VISIBLE);

            updateButton.setVisibility(View.GONE);

            txtEmailEdit.setVisibility(View.VISIBLE);
            txtStateEdit.setVisibility(View.VISIBLE);
            txtCityEdit.setVisibility(View.VISIBLE);
            txtAddressEdit.setVisibility(View.VISIBLE);
            txtPinEdit.setVisibility(View.VISIBLE);
            txtNameEdit.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_update) void updateUser(){
        mPresenter.updateUser(userInfo.userId,
                                userInfo.apiKey,
                                editTextName.getText().toString(),
                                editTextEmail.getText().toString(),
                                editTextState.getText().toString(),
                                editTextCity.getText().toString(),
                                editTextAddress.getText().toString(),
                                editTextPin.getText().toString()
                                );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
