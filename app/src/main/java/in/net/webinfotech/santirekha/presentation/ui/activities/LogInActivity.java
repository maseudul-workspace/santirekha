package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.presentation.presenters.LogInPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.LoginPresenterImpl;
import in.net.webinfotech.santirekha.presentation.routers.LogInRouter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class LogInActivity extends AppCompatActivity implements LogInPresenter.View, LogInRouter {

    @BindView(R.id.edit_text_email_login)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password_login)
    EditText editTextPassword;
    @BindView(R.id.txt_view_signup)
    TextView txtViewSignUp;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressbarLayout;
    LoginPresenterImpl mPresenter;
    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        txtViewSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUpIntent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        intialisePresenter();
    }

    public void intialisePresenter(){
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void loadProgressBar() {
        progressbarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressbarLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_log_in) void logIn(){
        if(editTextEmail.getText().toString().trim().isEmpty() || editTextPassword.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            Toast.makeText(this, "Please enter a valid mail address", Toast.LENGTH_SHORT).show();
        }else{
            loadProgressBar();
            mPresenter.loginUser(editTextEmail.getText().toString(), editTextPassword.getText().toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.txt_view_forget_pswd) void onForgetPasswordClicked(){
        Intent forgetPswdIntent = new Intent(this, ForgetPasswordActivity.class);
        startActivity(forgetPswdIntent);
    }

}
