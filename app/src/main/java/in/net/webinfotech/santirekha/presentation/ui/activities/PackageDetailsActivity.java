package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.presentation.presenters.PackageDetailsPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.PackageDetailsPresenterImpl;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;
import in.net.webinfotech.santirekha.util.GlideHelper;

public class PackageDetailsActivity extends AppCompatActivity implements PackageDetailsPresenter.View {

    @BindView(R.id.txt_view_package_detail_name)
    TextView txtViewPackageName;
    @BindView(R.id.txt_view_package_detail_description)
    TextView txtViewPackageDescription;
    @BindView(R.id.img_package_details_poster)
    ImageView imgViewPackagePoster;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    PackageDetailsPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_details);
        ButterKnife.bind(this);
        intialisePresenter();
        Intent intent = getIntent();
        int id = intent.getIntExtra("id",1);
        showLoadingProgress();
        mPresenter.getProductDetails(id);
    }

    public void intialisePresenter(){
        mPresenter = new PackageDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadProductDetails(Product product) {
        GlideHelper.setImageView(this, imgViewPackagePoster,getResources().getString(R.string.base_url) + "uploads/product_image/" + product.image);
        txtViewPackageName.setText(product.name);
        txtViewPackageDescription.setText(product.description);
    }

    @Override
    public void showLoadingProgress() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingProgress() {
        progressBarLayout.setVisibility(View.GONE);
    }
}
