package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.presentation.presenters.ProductDetailsPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.ProductDetailsPresenterImpl;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;
import in.net.webinfotech.santirekha.util.GlideHelper;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsPresenter.View{

    ProductDetailsPresenter mPresenter;
    @BindView(R.id.txtViewProductDetailName)
    TextView txtProductName;
    @BindView(R.id.txtViewProductDetailDescription)
    TextView txtDescription;
    @BindView(R.id.txtViewProductDetailPrice)
    TextView txtPrice;
    @BindView(R.id.numberPicker)
    NumberPicker numberPicker;
    @BindView(R.id.imgViewProductDetailPoster)
    ImageView imgPoster;
    @BindView(R.id.productDetailsBtnCart)
    Button btnCart;
    Product productDetails;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.product_details_relative_layout)
    RelativeLayout relativeLayout;
    AndroidApplication androidApplication;
    @BindView(R.id.txt_view_stock_status)
    TextView txtViewStockStatus;
    @BindView(R.id.txtProductSellingPrice)
    TextView txtViewSellingPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        int id = intent.getIntExtra("id",1);
        initialisePresenter();
        showLoadingProgress();
        mPresenter.getProductDetails(id);
    }

    public void initialisePresenter(){
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadProductDetails(final Product product) {
        this.productDetails = product;
        GlideHelper.setImageView(this, imgPoster,getResources().getString(R.string.base_url) + "uploads/product_image/" + product.image);
        txtProductName.setText(product.name);
        txtDescription.setText(product.description);
        txtPrice.setText("Rs " + product.price);
        if(product.mrp > 0){
            txtViewSellingPrice.setText("MRP " + product.mrp);
        }else{
            txtViewSellingPrice.setText("");
        }
        if(product.stock == 0){
            numberPicker.setValue(0);
            numberPicker.setMaxValue(0);
            txtViewStockStatus.setVisibility(View.VISIBLE);
            btnCart.setEnabled(false);
            btnCart.setText("Out of Stock");
        }else {
            numberPicker.setMaxValue(product.stock);
            numberPicker.setMinValue(1);
        }
        numberPicker.setWrapSelectorWheel(false);
        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(product.stock == 0){
                    Toast.makeText(getApplicationContext(), "Out of Stock", Toast.LENGTH_SHORT).show();
                }else {
                    androidApplication = (AndroidApplication) getApplicationContext();
                    if(androidApplication.getUserInfo(getApplicationContext()) == null){
                        showLogInSnackbar();
                    }else{
                        showLoadingProgress();
                        mPresenter.addToCart(productDetails.id, numberPicker.getValue());
                    }
                }
            }
        });

        txtViewSellingPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(relativeLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showLoadingProgress() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingProgress() {
        progressBarLayout.setVisibility(View.GONE);
    }
}
