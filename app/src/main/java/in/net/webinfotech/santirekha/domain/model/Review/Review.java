package in.net.webinfotech.santirekha.domain.model.Review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 28-02-2019.
 */

public class Review {

    @SerializedName("customer_name")
    @Expose
    public String customer_name;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("star")
    @Expose
    public int star;

    @SerializedName("created_at")
    @Expose
    public String created_at;

}
