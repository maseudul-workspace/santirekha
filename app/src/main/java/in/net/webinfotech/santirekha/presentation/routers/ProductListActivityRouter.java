package in.net.webinfotech.santirekha.presentation.routers;

/**
 * Created by Raj on 07-01-2019.
 */

public interface ProductListActivityRouter {
    void goToProductDetailsActivity(int id);
}
