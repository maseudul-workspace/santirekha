package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetProductListByCategoryInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetProductListByCategoryInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.presentation.presenters.PackageListActivityPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.routers.PackageListRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.PackageListAdapter;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 28-02-2019.
 */

public class PackageListPresenterImpl extends AbstractPresenter implements PackageListActivityPresenter,
                                                                            GetProductListByCategoryInteractor.Callback,
                                                                            PackageListAdapter.Callback
                                                                            {

    Context mContext;
    PackageListActivityPresenter.View mView;
    GetProductListByCategoryInteractorImpl mInteractor;
    PackageListAdapter adapter;
    PackageListRouter mRouter;
    Product[] newProducts;

    public PackageListPresenterImpl(Executor executor,
                                    MainThread mainThread,
                                    Context context,
                                    PackageListActivityPresenter.View view,
                                    PackageListRouter router
                                    ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void getProductList(int type, int pageNo, int categoryId, String state) {
        if(state.equals("refresh")){
            newProducts = null;
        }
        mInteractor = new GetProductListByCategoryInteractorImpl(mExecutor,
                mMainThread,
                this,
                new GetProductsRepositoryImpl(),
                type, pageNo, categoryId
        );
        mInteractor.execute();
    }

    @Override
    public void onGettingProductListSuccess(Product[] productLists, int totalPage) {
        Product[] tempProducts;
        tempProducts = newProducts;
        try {
            int len1 = tempProducts.length;
            int len2 = productLists.length;
            newProducts = new Product[len1 + len2];
            System.arraycopy(tempProducts, 0, newProducts, 0, len1);
            System.arraycopy(productLists, 0, newProducts, len1, len2);
            adapter.updateData(newProducts);
            adapter.notifyDataSetChanged();
            mView.hidePaginationProgressBar();
        }catch (NullPointerException e){
            newProducts = productLists;
            adapter = new PackageListAdapter(mContext, productLists, this);
            mView.loadProductList(adapter, totalPage);
            mView.hideProgressList();
        }
    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        mView.hideProgressList();
        Toast.makeText(mContext, "No packages found", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goToPackageDetails(int id) {
        mRouter.goToProductDetailsActivity(id);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

}
