package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.Wallet.WalletDetails;

/**
 * Created by Raj on 22-02-2019.
 */

public interface GetWalletHistoryInteractor {

    interface Callback{
        void onGettingWalletHistorySuccess(WalletDetails walletDetails);
        void onGettingWalletHistoryFail(String errorMsg);
    }

}
