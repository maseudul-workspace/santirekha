package in.net.webinfotech.santirekha.domain.model.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 19-02-2019.
 */

public class Cart {
    @SerializedName("cart_id")
    @Expose
    public int cartId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("stock")
    @Expose
    public int stock;

    @SerializedName("price")
    @Expose
    public int price;

    @SerializedName("quantity")
    @Expose
    public int quantity;

}
