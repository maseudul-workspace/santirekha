package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.User.UserInfo;

/**
 * Created by Raj on 13-02-2019.
 */

public interface LogInInteractor {
    interface Callback{
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
