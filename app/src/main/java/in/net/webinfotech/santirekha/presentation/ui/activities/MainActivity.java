package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.domain.model.Category.Category;
import in.net.webinfotech.santirekha.domain.model.ImageSliders.ImageSlider;
import in.net.webinfotech.santirekha.domain.model.Wallet.WalletHistory;
import in.net.webinfotech.santirekha.presentation.presenters.MainActivityPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.MainActitvityPresenterImpl;
import in.net.webinfotech.santirekha.presentation.routers.MainRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ReviewsAdapter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;
import in.net.webinfotech.santirekha.util.GlideHelper;

public class MainActivity extends BaseActivity implements MainActivityPresenter.View, MainRouter {

    @BindView(R.id.activity_main_viewflipper)
    ViewFlipper viewFlipper;
    @BindView(R.id.txtCartCount)
    TextView txtCartCount;
//    @BindView(R.id.packageCategorySpinner)
//    Spinner packageCategorySpinner;
    @BindView(R.id.pujaCategorySpinner)
    Spinner pujaCategorySpinner;
    @BindView(R.id.beverageCategorySpinner)
    Spinner beverageCategorySpinner;
    @BindView(R.id.menCategorySpinner)
    Spinner menCategorySpinner;
    @BindView(R.id.womenCategorySpinner)
    Spinner womenCategorySpinner;
    @BindView(R.id.babyCategorySpinner)
    Spinner babyCategorySpinner;
    @BindView(R.id.fastfoodCategorySpinner)
    Spinner fastfoodCategorySpinner;
    @BindView(R.id.educationCategorySpinner)
    Spinner educationCategorySpinner;
    @BindView(R.id.fruitsCategorySpinner)
    Spinner fruitsCategorySpinner;
    @BindView(R.id.groceryCategorySpinner)
    Spinner groceryCategorySpinner;
    @BindView(R.id.homeCategorySpinner)
    Spinner homeCategorySpinner;
    @BindView(R.id.app_rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.edit_text_review)
    EditText reviewEditText;
    @BindView(R.id.recycler_view_reviews)
    RecyclerView recyclerView;
    @BindView(R.id.main_relative_layout)
    RelativeLayout drawerLayout;
    @BindView(R.id.healthCategorySpinner)
    Spinner healthCategorySpinner;
    MainActitvityPresenterImpl mPresenter;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        initialisePresenter();
        setUpFlipper();
        mPresenter.getCategoriesList();
        mPresenter.getSliderImages();
    }

    public void initialisePresenter(){
        mPresenter = new MainActitvityPresenterImpl(ThreadExecutor.getInstance(),
                                                    MainThreadImpl.getInstance(),
                                                    this,
                                                    this,
                                                        this
                );
    }

    public void setUpFlipper(){
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    public void flipperImages(String image){
        ImageView imageView = new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageView(this, imageView, getResources().getString(R.string.base_url) + "uploads/slider_image/"  + image);
        viewFlipper.addView(imageView);
    }

    @OnClick(R.id.cart_layout) void goToCartDetailsActivity(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            Intent intent = new Intent(this, CartDetailsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void loadCartCount(int count) {
        if(count > 0){
            txtCartCount.setText(Integer.toString(count));
        }else{
            txtCartCount.setText("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getCartCount();
        mPresenter.checkUserStatus();
        mPresenter.fetchReviews();
    }

    void setCategorySpinners(Category[] categories){


        for(int i = 0; i < categories.length; i++){
            switch (categories[i].Id){
                case 1001:
                    final List<String> menSubCategoryCategoryList = new ArrayList<>();
                    final List<Integer> menSubCategoryIds = new ArrayList<>();
                    menSubCategoryCategoryList.add("Man Care");
                    menSubCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        menSubCategoryCategoryList.add(categories[i].subCategories[j].subCatName);
                        menSubCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> menCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, menSubCategoryCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    menCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    menCategorySpinner.setAdapter(menCategoryAdapter);
                    menCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, menSubCategoryIds.get(i));
                                menCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
                case 1002:
                    final List<String> fastfoodCategoryList = new ArrayList<>();
                    final List<Integer> fastfoodCategoryIds = new ArrayList<>();
                    fastfoodCategoryList.add("Fast Food");
                    fastfoodCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        fastfoodCategoryList.add(categories[i].subCategories[j].subCatName);
                        fastfoodCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> fastfoodCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, fastfoodCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    fastfoodCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fastfoodCategorySpinner.setAdapter(fastfoodCategoryAdapter);
                    fastfoodCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, fastfoodCategoryIds.get(i));
                                fastfoodCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
                case 1003:
                    final List<String> groceryCategoryList = new ArrayList<>();
                    final List<Integer> groceryCategoryIds = new ArrayList<>();
                    groceryCategoryList.add("Grocery");
                    groceryCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        groceryCategoryList.add(categories[i].subCategories[j].subCatName);
                        groceryCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> groceryCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, groceryCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    groceryCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    groceryCategorySpinner.setAdapter(groceryCategoryAdapter);
                    groceryCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, groceryCategoryIds.get(i));
                                groceryCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;

                case 1004:
                    //        Set women category
                    final List<String> womenCategoryList = new ArrayList<>();
                    final List<Integer> womenCategoryIds = new ArrayList<>();
                    womenCategoryList.add("Woman Care");
                    womenCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        womenCategoryList.add(categories[i].subCategories[j].subCatName);
                        womenCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }

                    final ArrayAdapter<String> womenCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, womenCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    womenCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    womenCategorySpinner.setAdapter(womenCategoryAdapter);
                    womenCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, womenCategoryIds.get(i));
                                womenCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
                case 1005:
                    //        Set education category
                    final List<String> educationCategoryList = new ArrayList<>();
                    final List<Integer> educationCategoryIds = new ArrayList<>();
                    educationCategoryList.add("Education");
                    educationCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        educationCategoryList.add(categories[i].subCategories[j].subCatName);
                        educationCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> educationCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, educationCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    educationCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    educationCategorySpinner.setAdapter(educationCategoryAdapter);
                    educationCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, educationCategoryIds.get(i));
                                educationCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
                case 1006:
                    //        Set homecare category
                    final List<String> homecareCategoryList = new ArrayList<>();
                    final List<Integer> homecareCategoryIds = new ArrayList<>();
                    homecareCategoryList.add("Home Care");
                    homecareCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        homecareCategoryList.add(categories[i].subCategories[j].subCatName);
                        homecareCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> homecareCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, homecareCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    homecareCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    homeCategorySpinner.setAdapter(homecareCategoryAdapter);
                    homeCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, homecareCategoryIds.get(i));
                                homeCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
                case 1007:
                    //        Set baby category
                    final List<String> babyCategoryList = new ArrayList<>();
                    final List<Integer> babyCategoryIds = new ArrayList<>();
                    babyCategoryList.add("Baby Care");
                    babyCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        babyCategoryList.add(categories[i].subCategories[j].subCatName);
                        babyCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }

                    final ArrayAdapter<String> babyCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, babyCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    babyCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    babyCategorySpinner.setAdapter(babyCategoryAdapter);
                    babyCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, babyCategoryIds.get(i));
                                babyCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
                case 1008:
                    //        Set fruits category
                    final List<String> fruitsCategoryList = new ArrayList<>();
                    final List<Integer> fruitsCategoryIds = new ArrayList<>();
                    fruitsCategoryList.add("Fruits");
                    fruitsCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        fruitsCategoryList.add(categories[i].subCategories[j].subCatName);
                        fruitsCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> fruitsCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, fruitsCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    fruitsCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fruitsCategorySpinner.setAdapter(fruitsCategoryAdapter);
                    fruitsCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, fruitsCategoryIds.get(i));
                                fruitsCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
//                case 1009:
//                    //        Set package category
//                    final List<String> packageCategoryList = new ArrayList<>();
//                    final List<Integer> packageCategoryIds = new ArrayList<>();
//                    packageCategoryList.add("Package");
//                    packageCategoryIds.add(0);
//                    for(int j = 0; j < categories[i].subCategories.length; j++){
//                        packageCategoryList.add(categories[i].subCategories[j].subCatName);
//                        packageCategoryIds.add(categories[i].subCategories[j].subCatId);
//                    }
//                    final ArrayAdapter<String> packageCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, packageCategoryList){
//                        @Override
//                        public boolean isEnabled(int position) {
//                            if(position == 0)
//                            {
//                                // Disable the first item from Spinner
//                                // First item will be use for hint
//                                return false;
//                            }
//                            else
//                            {
//                                return true;
//                            }
//                        }
//
//                        @Override
//                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//                            View view = super.getDropDownView(position, convertView, parent);
//                            TextView tv = (TextView) view;
//                            if(position == 0){
//                                // Set the hint text color gray
//                                tv.setTextColor(Color.GRAY);
//                            }
//                            else {
//                                tv.setTextColor(Color.BLACK);
//                            }
//                            return view;            }
//                    };
//                    packageCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                    packageCategorySpinner.setAdapter(packageCategoryAdapter);
//                    packageCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                            if(i>0) {
//
//                            }
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> adapterView) {
//
//                        }
//                    });
//                    break;
                case 1010:
                    final List<String> pujaCategoryList = new ArrayList<>();
                    final List<Integer> pujaCategoryIds = new ArrayList<>();
                    pujaCategoryList.add("Puja");
                    pujaCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        pujaCategoryList.add(categories[i].subCategories[j].subCatName);
                        pujaCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> pujaCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, pujaCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    pujaCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    pujaCategorySpinner.setAdapter(pujaCategoryAdapter);
                    pujaCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, pujaCategoryIds.get(i));
                                pujaCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
                case 1011:
                    //        Set beverage category
                    final List<String> beverageCategoryList = new ArrayList<>();
                    final List<Integer> beveraeCategoryIds = new ArrayList<>();
                    beverageCategoryList.add("Beverages");
                    beveraeCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        beverageCategoryList.add(categories[i].subCategories[j].subCatName);
                        beveraeCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> beverageCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, beverageCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    beverageCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    beverageCategorySpinner.setAdapter(beverageCategoryAdapter);
                    beverageCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, beveraeCategoryIds.get(i));
                                beverageCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
                case 1012:
                    //        Set healthcare category
                    final List<String> healthCategoryList = new ArrayList<>();
                    final List<Integer> healthCategoryIds = new ArrayList<>();
                    healthCategoryList.add("Health Care");
                    healthCategoryIds.add(0);
                    for(int j = 0; j < categories[i].subCategories.length; j++){
                        healthCategoryList.add(categories[i].subCategories[j].subCatName);
                        healthCategoryIds.add(categories[i].subCategories[j].subCatId);
                    }
                    final ArrayAdapter<String> healthCategoryAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, healthCategoryList){
                        @Override
                        public boolean isEnabled(int position) {
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;            }
                    };
                    healthCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    healthCategorySpinner.setAdapter(healthCategoryAdapter);
                    healthCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if(i > 0){
                                goToProductListActivity(2, healthCategoryIds.get(i));
                                healthCategorySpinner.setSelection(0);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    break;
            }
        }
    }


    @Override
    public void loadCategories(Category[] categories) {
        setCategorySpinners(categories);
//        setCategories(categories);
    }

    public void goToProductListActivity(int type, int categoryId){
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("type",type);
        intent.putExtra("categoryId", categoryId);
        startActivity(intent);
    }

    @OnClick(R.id.txt_view_wallet) void onWalletClick(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            Intent intent = new Intent(this, WalletHistoryActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.txt_view_business) void onBusinessClicked(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            Intent intent = new Intent(this, DownlineActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.txt_view_package) void onPackageClick(){
        Intent intent = new Intent(this, PackageListActivity.class);
        intent.putExtra("type",1);
        intent.putExtra("categoryId", 1009);
        startActivity(intent);
    }

    @OnClick(R.id.btn_submit_review) void onSubmitBtnClicked(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) == null){
            showLogInSnackbar();
        }else{
            if(reviewEditText.getText().toString().isEmpty()){
                Toast.makeText(this, "Please give your feedback", Toast.LENGTH_SHORT).show();
            }else if(ratingBar.getRating() < 1){
                Toast.makeText(this, "Please give rating", Toast.LENGTH_SHORT).show();
            }else {
                mPresenter.submitReview(Math.round(ratingBar.getRating()), reviewEditText.getText().toString());
            }
        }
    }

    @Override
    public void loadReviews(ReviewsAdapter adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void loadSliderImages(ImageSlider[] imageSliders) {
        for(int i = 0; i < imageSliders.length; i++){
            flipperImages(imageSliders[i].image);
        }
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(drawerLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(loginIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void goToLogin() {
        Intent loginIntent = new Intent(getApplicationContext(), LogInActivity.class);
        startActivity(loginIntent);
        finish();
    }
}
