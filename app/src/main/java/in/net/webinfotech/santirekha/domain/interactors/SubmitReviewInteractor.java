package in.net.webinfotech.santirekha.domain.interactors;

/**
 * Created by Raj on 28-02-2019.
 */

public interface SubmitReviewInteractor {
    interface Callback{
        void onSubmitReviewSuccess(String successMsg);
        void onSubmitReviewFail(String errorMsg);
    }
}
