package in.net.webinfotech.santirekha.domain.interactors.impl;

import android.util.Log;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetProductDetailsByIdInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.domain.model.Products.ProductDetailsWrapper;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 07-01-2019.
 */

public class GetProductDetailsByIdInteractorImpl extends AbstractInteractor implements GetProductDetailsByIdInteractor {

    GetProductsRepositoryImpl mRepository;
    Callback mCallback;
    int productId;

    public GetProductDetailsByIdInteractorImpl(Executor threadExecutor, MainThread mainThread,
                                               Callback callback,
                                               GetProductsRepositoryImpl repository,
                                               int productId) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.productId = productId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final Product productList){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsSuccess(productList);
            }
        });
    }

    @Override
    public void run() {
        final ProductDetailsWrapper productDetailsWrapper = mRepository.getProductDetailsById(productId);
        if(productDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!productDetailsWrapper.status){
            notifyError(productDetailsWrapper.message);
        }else{
           postMessage(productDetailsWrapper.products);
        }
    }
}
