package in.net.webinfotech.santirekha.domain.model.ImageSliders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 24-06-2019.
 */

public class ImageSliderWrapper {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public ImageSlider[] imageSliders;
}
