package in.net.webinfotech.santirekha.domain.model.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-02-2019.
 */

public class WalletDetails {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("amount")
    @Expose
    public double amount;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("history")
    @Expose
    public WalletHistory[] walletHistories;
}
