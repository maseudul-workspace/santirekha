package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.domain.model.Cart.Cart;
import in.net.webinfotech.santirekha.presentation.presenters.CartDetailsActivityPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.CartDetailsActivityPresenterImpl;
import in.net.webinfotech.santirekha.presentation.routers.CartIDetailsActivityRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.CartItemListAdapter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;
import in.net.webinfotech.santirekha.util.GlideHelper;

public class CartDetailsActivity extends BaseActivity implements CartDetailsActivityPresenter.View,
        CartIDetailsActivityRouter {

    @BindView(R.id.recyclerviewCartItemList)
    RecyclerView recyclerView;
    @BindView(R.id.txtCartSubtotal)
    TextView txtSubtotal;
    @BindView(R.id.btnCartCheckout)
    Button btnCheckout;
    CartDetailsActivityPresenterImpl mPresenter;
    Cart[] carts;
    AlertDialog dialog;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_cart_item_list);
        initialisePresenter();
    }

    public void initialisePresenter(){
        mPresenter = new CartDetailsActivityPresenterImpl(ThreadExecutor.getInstance(),
                                                            MainThreadImpl.getInstance(),
                                                            this,
                                                            this,
                                                            this);
    }

    @Override
    public void loadCartItemList(CartItemListAdapter adapter, Cart[] carts) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        this.carts = carts;
    }

    @Override
    public void loadCartSubtotal(int subtotal, int itemCount, boolean status) {
        if(subtotal == 0 && itemCount == 0){
            txtSubtotal.setText("");
            btnCheckout.setVisibility(View.GONE);
        }else{
            if(status){
                txtSubtotal.setText("Cart Subtotal " + "(" + itemCount + "items) Rs. " + subtotal);
                btnCheckout.setVisibility(View.VISIBLE);
                btnCheckout.setText("Proceed to checkout");
                btnCheckout.setEnabled(true);
            }else {
                txtSubtotal.setText("Cart Subtotal " + "(" + itemCount + "items) Rs. " + subtotal);
                btnCheckout.setVisibility(View.VISIBLE);
                btnCheckout.setText("Please update cart to proceed");
                btnCheckout.setEnabled(false);
            }

        }
    }

    @Override
    public void showUpdateDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.update_cart_dialog, null);
        ImageView productPoster = (ImageView) view.findViewById(R.id.imgViewUpdateProductPoster);
        TextView productName = (TextView) view.findViewById(R.id.txtViewUpdateProductName);
        TextView productPrice = (TextView) view.findViewById(R.id.txtViewProductUpdatePrice);
        final NumberPicker numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker);
        Button updateButton = (Button) view.findViewById(R.id.updateCartBtn);
        GlideHelper.setImageView(this, productPoster, getResources().getString(R.string.base_url) + "uploads/product_image/" + carts[position].image);
        productName.setText(carts[position].name);
        productPrice.setText("Rs." + carts[position].price);
        numberPicker.setMaxValue(carts[position].stock);
        numberPicker.setMinValue(1);
        numberPicker.setValue(carts[position].quantity);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.updateCart(carts[position].cartId, numberPicker.getValue());
                dialog.dismiss();
                showLoadingProgress();
            }
        });
        builder.setView(view);
        dialog = builder.create();
        dialog.show();

    }

    @Override
    public void goToProductDetailsActivity(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("id",productId);
        startActivity(intent);
    }

    @Override
    public void showLoadingProgress() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingProgress() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnCartCheckout) void goToOrderDetails(){
        Intent intent = new Intent(this, OrderSummaryActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoadingProgress();
        mPresenter.getCartList();
    }
}
