package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.presentation.presenters.ForgetPasswordPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.ForgetPasswordPresenterImpl;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class ForgetPasswordActivity extends AppCompatActivity implements ForgetPasswordPresenter.View{

    @BindView(R.id.loader_layout)
    View loaderLayout;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    ForgetPasswordPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        initialisePresenter();
    }

    public void initialisePresenter(){
        mPresenter = new ForgetPasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void showLoader() {
        loaderLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        loaderLayout.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked(){
        if(editTextEmail.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Field cannot be empty", Toast.LENGTH_SHORT).show();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            Toast.makeText(this, "Please enter a valid email", Toast.LENGTH_SHORT).show();
        }else{
            mPresenter.submitEmail(editTextEmail.getText().toString());
            showLoader();
        }
    }

}
