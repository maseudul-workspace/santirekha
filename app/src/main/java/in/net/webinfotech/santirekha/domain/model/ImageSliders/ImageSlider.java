package in.net.webinfotech.santirekha.domain.model.ImageSliders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 24-06-2019.
 */

public class ImageSlider {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("image")
    @Expose
    public String image;

}
