package in.net.webinfotech.santirekha.domain.interactors.impl;

import android.util.Log;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.UpdateCartInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Cart.CartUpdateResponse;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;

/**
 * Created by Raj on 09-01-2019.
 */

public class UpdateCartInteractorImpl extends AbstractInteractor implements UpdateCartInteractor {

    CartRepositoryImpl mRepository;
    Product productDetails;
    Callback mCallback;
    int quantity;
    int userId;
    int cartId;
    String apiKey;

    public UpdateCartInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    CartRepositoryImpl repository,
                                    Callback callback,
                                    int quantity,
                                    int userId,
                                    int cartId,
                                    String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.quantity = quantity;
        this.userId = userId;
        this.cartId = cartId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateCartFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateCartSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final CartUpdateResponse updateResponse = mRepository.updateCart(userId, apiKey, cartId, quantity);
        if(updateResponse == null){
            notifyError("Something went wrong");
        }else if(!updateResponse.status){
            notifyError(updateResponse.message);
        } else{
            postMessage(updateResponse.message);
        }
    }
}
