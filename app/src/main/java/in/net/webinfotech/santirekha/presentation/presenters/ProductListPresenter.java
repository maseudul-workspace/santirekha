package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ProductListAdapter;

/**
 * Created by Raj on 05-01-2019.
 */

public interface ProductListPresenter extends BasePresenter{
    void getProductList(int type, int pageNo, int categoryId, String state);
    interface View{
        void loadProductList(ProductListAdapter adapter, int totalPageCount);
        void hideProgressList();
        void showProgressList();
        void showLoginSnackbar();
        void showPaginationProgressBar();
        void hidePaginationProgressBar();
    }
}
