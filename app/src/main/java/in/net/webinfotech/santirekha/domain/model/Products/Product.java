package in.net.webinfotech.santirekha.domain.model.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 05-01-2019.
 */

public class Product {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("category")
    @Expose
    public String category;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("stock")
    @Expose
    public int stock;

    @SerializedName("mrp")
    @Expose
    public int mrp;

    @SerializedName("price")
    @Expose
    public int price;

    @SerializedName("image")
    @Expose
    public String image;


}
