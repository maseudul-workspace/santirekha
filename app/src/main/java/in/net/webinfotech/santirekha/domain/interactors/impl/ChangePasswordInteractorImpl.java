package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.ChangePasswordInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.PasswordChangeResponse;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 14-02-2019.
 */

public class ChangePasswordInteractorImpl extends AbstractInteractor implements ChangePasswordInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String newPassword;
    String oldPassword;
    String apiKey;
    int userId;

    public ChangePasswordInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        UserRepositoryImpl repository,
                                        String newPassword,
                                                String oldPassword,
                                                String apiKey,
                                                int userId
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.newPassword = newPassword;
        this.oldPassword = oldPassword;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangingPasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangingPasswordSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final PasswordChangeResponse passwordChangeResponse = mRepository.changePassword(newPassword, oldPassword, apiKey, userId);
        if(passwordChangeResponse == null){
            notifyError("Something went wrong");
        }else if(!passwordChangeResponse.status){
            notifyError(passwordChangeResponse.message);
        }else{
            postMessage(passwordChangeResponse.message);
        }
    }
}
