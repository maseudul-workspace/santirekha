package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 14-02-2019.
 */

public interface ChangePasswordPresenter extends BasePresenter {
    void changePassword(String newPassword, String oldPassword, String apiKey, int userId);
    interface View{
        void loadProgressBar();
        void hideProgressBar();
    }
}
