package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.presentation.presenters.SignUpPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.SignUpPresenterImpl;
import in.net.webinfotech.santirekha.presentation.routers.SignUpRouter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class SignUpActivity extends AppCompatActivity implements SignUpRouter, SignUpPresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_refer_code)
    EditText editTextReferCode;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    SignUpPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        initalisePresenter();
    }

    public void initalisePresenter(){
        mPresenter = new SignUpPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @OnClick(R.id.btn_sign_up) void onSignUpClick(){

        if(editTextEmail.getText().toString().trim().isEmpty() ||
                editTextName.getText().toString().trim().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty()
                )
        {
            Toast.makeText(this, "All fields are mandatory except refer code", Toast.LENGTH_SHORT).show();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            Toast.makeText(this, "Please give a valid email", Toast.LENGTH_SHORT).show();
        }else{
            mPresenter.createUser(editTextName.getText().toString(),
                                editTextPhone.getText().toString(),
                                editTextEmail.getText().toString(),
                                editTextPassword.getText().toString(),
                                "",
                                "",
                                "",
                                editTextReferCode.getText().toString(),
                                ""
                                );
            showProgressBar();
        }
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
