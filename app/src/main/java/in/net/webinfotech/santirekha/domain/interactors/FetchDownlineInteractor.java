package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.User.Downline;

/**
 * Created by Raj on 27-02-2019.
 */

public interface FetchDownlineInteractor {
    interface Callback{
        void onGettingDownlineListSuccess(Downline[] downlines);
        void onGettingDownlineListFail(String errorMsg);
    }
}
