package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.AddShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.DeleteShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.FetchShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.UpdateShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.AddShippingAddressInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.DeleteShippingAddressInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.UpdateShippingAddressInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;
import in.net.webinfotech.santirekha.presentation.presenters.ShippingAddressPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ShippingAddressAdapter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 15-02-2019.
 */

public class ShippingAddressPresenterImpl extends AbstractPresenter implements ShippingAddressPresenter,
                                                                                FetchShippingAddressInteractor.Callback,
                                                                                AddShippingAddressInteractor.Callback,
                                                                                ShippingAddressAdapter.Callback,
                                                                                UpdateShippingAddressInteractor.Callback,
                                                                                DeleteShippingAddressInteractor.Callback
{

    Context mContext;
    FetchShippingAddressInteractorImpl mInteractor;
    ShippingAddressPresenter.View mView;
    ShippingAddressAdapter adapter;
    ShippingAddress[] addresses;
    AddShippingAddressInteractorImpl addressInteractor;
    int userId;
    String apiKey;
    UpdateShippingAddressInteractorImpl updateShippingAddressInteractor;
    DeleteShippingAddressInteractorImpl deleteShippingAddressInteractor;

    public ShippingAddressPresenterImpl(Executor executor,
                                        MainThread mainThread,
                                        Context context,
                                        ShippingAddressPresenter.View view
                                        ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }


    @Override
    public void getShippingAddress(int userId, String apiKey) {
        this.userId = userId;
        this.apiKey = apiKey;
        mInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), apiKey, userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingShippingAddressFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingShippingAddressSuccess(ShippingAddress[] shippingAddresses) {
        addresses = shippingAddresses;
        adapter = new ShippingAddressAdapter(mContext, shippingAddresses, this, false);
        mView.setRecyclerViewAdapter(adapter);
        adapter.notifyDataSetChanged();
        mView.hideProgressBar();
    }

    @Override
    public void addShippingAddress(int userId, String apiKey, String mobile, String email, String state, String city, String address, String pin) {
        addressInteractor = new AddShippingAddressInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(),
                                                                    userId, apiKey, mobile, email, state, city, address, pin);
        addressInteractor.execute();
    }

    @Override
    public void onShippingAddressAddSuccess(String successMsg) {
        mView.showProgresBar();
        getShippingAddress(this.userId, this.apiKey);
    }

    @Override
    public void onShippingAddressAddFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEditClick(int position, int id) {
        ShippingAddress shippingAddress = addresses[position];
        mView.loadEditDialog(shippingAddress);
    }

    @Override
    public void updateAddress(int userId, String apiKey, int addressId, String mobile, String email, String state, String city, String address, String pin) {
        updateShippingAddressInteractor = new UpdateShippingAddressInteractorImpl(mExecutor, mMainThread, this,
                                                                                    new UserRepositoryImpl(), userId, apiKey,
                                                                                    addressId, mobile, email, state, city, address, pin);
        updateShippingAddressInteractor.execute();
    }

    @Override
    public void onUpdateAddressSuccess(String successMsg) {
        mView.showProgresBar();
        getShippingAddress(this.userId, this.apiKey);
    }

    @Override
    public void onUpdateAddressFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteClick(int id) {
        mView.showProgresBar();
        deleteShippingAddressInteractor = new DeleteShippingAddressInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), apiKey, userId, id);
        deleteShippingAddressInteractor.execute();
    }

    @Override
    public void onAddressDeleteSucess(String successMsg) {
        mView.showProgresBar();
        getShippingAddress(this.userId, this.apiKey);
    }

    @Override
    public void onAddressDeleteFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onSetBtnClicked(ShippingAddress shippingAddress) {

    }
}
