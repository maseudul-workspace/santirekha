package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.User.UserInfo;

/**
 * Created by Raj on 12-02-2019.
 */

public interface SignUpInteractor {
    interface Callback{
        void onSignUpSuccess(UserInfo userInfo);
        void onSignUpFail(String errorMsg);
    }
}
