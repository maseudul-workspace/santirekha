package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.UpdateShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.UpdateShippingAddressResponse;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 15-02-2019.
 */

public class UpdateShippingAddressInteractorImpl extends AbstractInteractor implements UpdateShippingAddressInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    int userId;
    String apiKey;
    String mobile;
    String email;
    String state;
    String city;
    String address;
    String pin;
    int addressId;

    public UpdateShippingAddressInteractorImpl(Executor threadExecutor,
                                               MainThread mainThread,
                                               Callback callback,
                                               UserRepositoryImpl repository,
                                               int userId,
                                               String apiKey,
                                               int addressId,
                                               String mobile,
                                               String email,
                                               String state,
                                               String city,
                                               String address,
                                               String pin) {
        super(threadExecutor, mainThread);

        this.mCallback = callback;
        this.mRepository = repository;
        this.userId = userId;
        this.apiKey = apiKey;
        this.email = email;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pin = pin;
        this.mobile = mobile;
        this.addressId = addressId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateAddressFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateAddressSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final UpdateShippingAddressResponse updateShippingAddressResponse = mRepository.updateShippingAddress( userId, apiKey, addressId, mobile, email, state, city, address, pin);
        if(updateShippingAddressResponse == null){
            notifyError("Something went wrong");
        }else if(!updateShippingAddressResponse.status){
            notifyError(updateShippingAddressResponse.message);
        }else{
            postMessage(updateShippingAddressResponse.message);
        }
    }
}
