package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 13-02-2019.
 */

public interface LogInPresenter extends BasePresenter {
    void loginUser(String email, String password);
    interface View{
        void loadProgressBar();
        void hideProgressBar();
    }
}
