package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetUserDetailsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.UserDetails;
import in.net.webinfotech.santirekha.domain.model.User.UserDetailsWrapper;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 13-02-2019.
 */

public class GetUserDetailsInteractorImpl extends AbstractInteractor implements GetUserDetailsInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public GetUserDetailsInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        UserRepositoryImpl repository,
                                        String apiKey,
                                        int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserDetails userDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsSuccess(userDetails);
            }
        });
    }

    @Override
    public void run() {
        final UserDetailsWrapper userDetailsWrapper = mRepository.fetchUserDetails(userId, apiKey);
        if(userDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!userDetailsWrapper.status){
            notifyError(userDetailsWrapper.message);
        }else{
            postMessage(userDetailsWrapper.userDetails);
        }
    }
}
