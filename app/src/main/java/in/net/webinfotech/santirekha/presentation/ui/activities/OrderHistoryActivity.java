package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.presentation.presenters.OrderHistoryPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.OrderHistoryPresenterImpl;
import in.net.webinfotech.santirekha.presentation.ui.adapters.OrdersHistoryAdapter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class OrderHistoryActivity extends AppCompatActivity implements OrderHistoryPresenter.View {

    @BindView(R.id.recycler_view_orders)
    RecyclerView recyclerView;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    OrderHistoryPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        ButterKnife.bind(this);
        initialisePresenter();
        showProgressBar();
        mPresenter.getOrderHistory();
    }

    public void initialisePresenter(){
        mPresenter = new OrderHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadOrdersAdapter(OrdersHistoryAdapter ordersHistoryAdapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(ordersHistoryAdapter);
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }
}
