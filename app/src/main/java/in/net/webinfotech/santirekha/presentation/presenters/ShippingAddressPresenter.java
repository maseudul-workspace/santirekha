package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;
import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ShippingAddressAdapter;

/**
 * Created by Raj on 15-02-2019.
 */

public interface ShippingAddressPresenter extends BasePresenter {
    void getShippingAddress(int userId, String apiKey);
    void addShippingAddress( int userId,
                             String apiKey,
                             String mobile,
                             String email,
                             String state,
                             String city,
                             String address,
                             String pin);
    void updateAddress( int userId,
                        String apiKey,
                        int addressId,
                        String mobile,
                        String email,
                        String state,
                        String city,
                        String address,
                        String pin);
    interface View{
        void showProgresBar();
        void hideProgressBar();
        void setRecyclerViewAdapter(ShippingAddressAdapter adapter);
        void loadEditDialog(ShippingAddress shippingAddress);
    }
}
