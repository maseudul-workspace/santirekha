package in.net.webinfotech.santirekha.domain.interactors.impl;

import android.util.Log;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetCartDetailsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Cart.Cart;
import in.net.webinfotech.santirekha.domain.model.Cart.CartListWrapper;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.repository.ProductRepository.CartRepository;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;

/**
 * Created by Raj on 10-01-2019.
 */

public class GetCartDetailsInteractorImpl extends AbstractInteractor implements GetCartDetailsInteractor {

    Callback mCallback;
    CartRepositoryImpl mRepository;
    int userId;
    String apiKey;

    public GetCartDetailsInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        CartRepositoryImpl repository,
                                        Callback callback,
                                        int userId,
                                        String apiKey
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGetCartDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final Cart[] carts){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGetCartDetailsSuccess(carts);
            }
        });
    }

    @Override
    public void run() {
        final CartListWrapper cartListWrapper = mRepository.getCartDetails(userId, apiKey);
        if(cartListWrapper == null){
            notifyError("Something went wrong");
        }else if(!cartListWrapper.status){
            notifyError(cartListWrapper.message);
        }
        else{
            postMessage(cartListWrapper.carts);
        }
    }
}
