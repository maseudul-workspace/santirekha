package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetProductDetailsByIdInteractor;
import in.net.webinfotech.santirekha.domain.interactors.UpdateCartInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.AddToCartInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetProductDetailsByIdInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.UpdateCartInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.ProductDetailsPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 07-01-2019.
 */

public class ProductDetailsPresenterImpl extends AbstractPresenter implements ProductDetailsPresenter,
                                                                                GetProductDetailsByIdInteractor.Callback,
                                                                                AddToCartInteractor.Callback{


    Context mContext;
    GetProductDetailsByIdInteractorImpl mInteractor;
    AddToCartInteractorImpl mCartInteractor;
    UpdateCartInteractorImpl mUpdateInteractor;
    ProductDetailsPresenter.View mView;
    AndroidApplication androidApplication;
    UserInfo userInfo;

    public ProductDetailsPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       ProductDetailsPresenter.View view) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getProductDetails(int productId) {

        mInteractor = new GetProductDetailsByIdInteractorImpl(mExecutor,
                                                                mMainThread,
                                                                this,
                                                                new GetProductsRepositoryImpl(),
                                                                productId);
        mInteractor.execute();
    }

    @Override
    public void onGettingProductDetailsSuccess(Product productLists) {
        this.mView.loadProductDetails(productLists);
        mView.hideLoadingProgress();
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideLoadingProgress();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void addToCart(int productId, int quantity) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext.getApplicationContext());
        String apiKey = userInfo.apiKey;
        int userId = userInfo.userId;
        mCartInteractor = new AddToCartInteractorImpl(mExecutor,
                                                        mMainThread,
                                                        new CartRepositoryImpl(),
                                                        this,
                                                        apiKey,
                                                        productId,
                                                        userId,
                                                        quantity
                                                        );
        mCartInteractor.execute();
    }

    @Override
    public void onAddToCartSuccess(String successMsg) {
        mView.hideLoadingProgress();
        Toast.makeText(mContext, "Successfully added to cart", Toast.LENGTH_LONG).show();
//        onGettingProductDetailsSuccess(productDetails);
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.hideLoadingProgress();
        if(errorMsg.equals("GoToCart")){
            Toast.makeText(mContext, "Already added to cart", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
