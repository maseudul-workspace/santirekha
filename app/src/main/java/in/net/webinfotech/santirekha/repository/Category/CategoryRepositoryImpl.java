package in.net.webinfotech.santirekha.repository.Category;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.santirekha.domain.model.Category.CategoryListWrapper;
import in.net.webinfotech.santirekha.domain.model.ImageSliders.ImageSliderWrapper;
import in.net.webinfotech.santirekha.repository.ApiClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 11-02-2019.
 */

public class CategoryRepositoryImpl {
    CategoryRepository mRepository;

    public CategoryRepositoryImpl() {
        mRepository = ApiClient.createService(CategoryRepository.class);
    }

    public CategoryListWrapper getCategoryList(){
        CategoryListWrapper categoryListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> categoryList = mRepository.getCategoryList();

            Response<ResponseBody> response = categoryList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    categoryListWrapper = null;
                }else{
                    categoryListWrapper = gson.fromJson(responseBody, CategoryListWrapper.class);

                }
            } else {
                categoryListWrapper = null;
            }
        }catch (Exception e){
            categoryListWrapper = null;
        }
        return categoryListWrapper;
    }

    public ImageSliderWrapper getImageSliders(){
        ImageSliderWrapper imageSliderWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> images = mRepository.getImageSliders();

            Response<ResponseBody> response = images.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    imageSliderWrapper = null;
                }else{
                    imageSliderWrapper = gson.fromJson(responseBody, ImageSliderWrapper.class);

                }
            } else {
                imageSliderWrapper = null;
            }
        }catch (Exception e){
            imageSliderWrapper = null;
        }
        return imageSliderWrapper;
    }

}
