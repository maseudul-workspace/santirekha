package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Cart.CartAddedReponse;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;

/**
 * Created by Raj on 09-01-2019.
 */

public class AddToCartInteractorImpl extends AbstractInteractor implements AddToCartInteractor {

    CartRepositoryImpl mRepository;
    Callback mCallback;
    int quantity;
    int userId;
    int productId;
    String apiKey;

    public AddToCartInteractorImpl(Executor threadExecutor,
                                   MainThread mainThread,
                                   CartRepositoryImpl repository,
                                   Callback callback,
                                   String apiKey,
                                   int productId,
                                   int userId,
                                   int quantity) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.quantity = quantity;
        this.userId = userId;
        this.productId = productId;
        this.apiKey = apiKey;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final CartAddedReponse addedReponse = mRepository.addToCart(apiKey, userId, productId, quantity);;
        if(addedReponse == null){
            notifyError("Something went wrong");
        }else if(!addedReponse.status && addedReponse.code == 200){
            notifyError("GoToCart");
        }else if(!addedReponse.status){
            notifyError(addedReponse.message);
        }
        else{
            postMessage(addedReponse.message);
        }
    }
}
