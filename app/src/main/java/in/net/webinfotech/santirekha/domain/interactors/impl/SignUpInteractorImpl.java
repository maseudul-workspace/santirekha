package in.net.webinfotech.santirekha.domain.interactors.impl;

import android.content.Context;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.SignUpInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.domain.model.User.UserInfoWrapper;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 12-02-2019.
 */

public class SignUpInteractorImpl extends AbstractInteractor implements SignUpInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String name;
    String mobile;
    String email;
    String password;
    String state;
    String city;
    String address;
    String referalCode;
    String pin;

    public SignUpInteractorImpl(Executor threadExecutor,
                                MainThread mainThread,
                                Callback callback,
                                UserRepositoryImpl repository,
                                String name,
                                String mobile,
                                String email,
                                String password,
                                String state,
                                String city,
                                String address,
                                String referalCode,
                                String pin
                                ) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.state = state;
        this.city = city;
        this.address = address;
        this.referalCode = referalCode;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSignUpFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSignUpSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.signUp(name, mobile, email, password, state, city, address, referalCode, pin);
        if(userInfoWrapper == null){
            notifyError("Something went wrong");
        }else if(!userInfoWrapper.status){
            notifyError(userInfoWrapper.message);
        }else{
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
