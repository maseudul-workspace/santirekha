package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.ChangePasswordInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.ChangePasswordInteractorImpl;
import in.net.webinfotech.santirekha.presentation.presenters.ChangePasswordPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 14-02-2019.
 */

public class ChangePasswordPresenterImpl extends AbstractPresenter implements ChangePasswordPresenter, ChangePasswordInteractor.Callback{

    Context mContext;
    ChangePasswordInteractorImpl mInteractor;
    ChangePasswordPresenter.View mView;

    public ChangePasswordPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       ChangePasswordPresenter.View view
                                       ) {
        super(executor, mainThread);
        this.mView = view;
        this.mContext = context;
    }

    @Override
    public void changePassword(String newPassword, String oldPassword, String apiKey, int userId) {
        mInteractor = new ChangePasswordInteractorImpl(mExecutor, mMainThread, this,
                                                        new UserRepositoryImpl(), newPassword, oldPassword, apiKey, userId);
        mInteractor.execute();
    }

    @Override
    public void onChangingPasswordSuccess(String successMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, successMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onChangingPasswordFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
