package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.ChangePasswordPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.ChangePasswordPresenterImpl;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordPresenter.View{

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edit_text_old_password)
    EditText editTextOldPassword;
    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPassword;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    ChangePasswordPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        intialisePresenter();
    }

    public void intialisePresenter(){
        mPresenter = new ChangePasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_change_password) void changePassword(){
        if(editTextNewPassword.getText().toString().trim().isEmpty() || editTextNewPassword.getText().toString().trim().isEmpty() ){
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
        }else{
            loadProgressBar();
            mPresenter.changePassword(editTextNewPassword.getText().toString(), editTextOldPassword.getText().toString(), userInfo.apiKey, userInfo.userId);
        }
    }

    @Override
    public void loadProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
