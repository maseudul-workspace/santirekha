package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.LogInInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.LogInInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.LogInPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.routers.LogInRouter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 13-02-2019.
 */

public class LoginPresenterImpl extends AbstractPresenter implements LogInPresenter,
                                                                        LogInInteractor.Callback{

    Context mContext;
    LogInInteractorImpl mInteractor;
    LogInPresenter.View mView;
    AndroidApplication androidApplication;
    LogInRouter mRouter;

    public LoginPresenterImpl(Executor executor,
                              MainThread mainThread,
                              Context context,
                              LogInPresenter.View view,
                              LogInRouter router
                              ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
        mRouter = router;
    }

    @Override
    public void loginUser(String email, String password) {
        mInteractor = new LogInInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, email, password);
        mInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        mView.hideProgressBar();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        Toast.makeText(mContext, "Login Successful", Toast.LENGTH_SHORT).show();
        this.mRouter.goToMain();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
