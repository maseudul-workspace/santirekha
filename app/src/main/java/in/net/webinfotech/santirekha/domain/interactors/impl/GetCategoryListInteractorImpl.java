package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetCategoryListInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Category.Category;
import in.net.webinfotech.santirekha.domain.model.Category.CategoryListWrapper;
import in.net.webinfotech.santirekha.repository.Category.CategoryRepositoryImpl;

/**
 * Created by Raj on 11-02-2019.
 */

public class GetCategoryListInteractorImpl extends AbstractInteractor implements GetCategoryListInteractor {

    Callback mCallback;
    CategoryRepositoryImpl mRepository;

    public GetCategoryListInteractorImpl(Executor threadExecutor,
                                         MainThread mainThread,
                                         Callback callback,
                                         CategoryRepositoryImpl repository
                                          ) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCategoryListFail(errorMsg);
            }
        });
    }

    private void postMessage(final Category[] categories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCategoryListSuccess(categories);
            }
        });
    }

    @Override
    public void run() {
        CategoryListWrapper categoryListWrapper = mRepository.getCategoryList();
        if(categoryListWrapper == null){
            notifyError("Something went wrong");
        }else if(!categoryListWrapper.status){
            notifyError(categoryListWrapper.message);
        }else{
            postMessage(categoryListWrapper.categories);
        }
    }
}
