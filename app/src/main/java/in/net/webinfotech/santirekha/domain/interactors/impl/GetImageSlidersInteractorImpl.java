package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetImageSlidersInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.ImageSliders.ImageSlider;
import in.net.webinfotech.santirekha.domain.model.ImageSliders.ImageSliderWrapper;
import in.net.webinfotech.santirekha.repository.Category.CategoryRepositoryImpl;

/**
 * Created by Raj on 24-06-2019.
 */

public class GetImageSlidersInteractorImpl extends AbstractInteractor implements GetImageSlidersInteractor {

    Callback mCallback;
    CategoryRepositoryImpl mRepository;

    public GetImageSlidersInteractorImpl(Executor threadExecutor,
                                         MainThread mainThread,
                                         Callback callback,
                                         CategoryRepositoryImpl repository
                                         ) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.ongettingImageSliderFail(errorMsg);
            }
        });
    }

    private void postMessage(final ImageSlider[] imageSliders){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingImageSlidersSuccess(imageSliders);
            }
        });
    }

    @Override
    public void run() {
        final ImageSliderWrapper imageSliderWrapper = mRepository.getImageSliders();
        if(imageSliderWrapper == null){
            notifyError("Something went wrong");
        }else if(!imageSliderWrapper.status){
            notifyError(imageSliderWrapper.message);
        }else{
            postMessage(imageSliderWrapper.imageSliders);
        }
    }
}
