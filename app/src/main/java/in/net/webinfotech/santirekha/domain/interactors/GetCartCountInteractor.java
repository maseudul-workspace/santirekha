package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.interactors.base.Interactor;

/**
 * Created by Raj on 09-01-2019.
 */

public interface GetCartCountInteractor extends Interactor {
    interface Callback{
        void onGetCartCountSuccess(int count);
        void onGetCartCountFail(String errorMsg);
    }
}
