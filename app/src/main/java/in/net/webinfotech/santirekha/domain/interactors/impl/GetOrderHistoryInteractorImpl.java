package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetOrderHistoryInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Orders.OrderHistory;
import in.net.webinfotech.santirekha.domain.model.Orders.OrderHistoryWrapper;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 22-02-2019.
 */

public class GetOrderHistoryInteractorImpl extends AbstractInteractor implements GetOrderHistoryInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public GetOrderHistoryInteractorImpl(Executor threadExecutor,
                                         MainThread mainThread,
                                         Callback callback,
                                         UserRepositoryImpl repository,
                                         String apiKey,
                                         int userId
                                         ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(final OrderHistory[] orderHistories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistorySuccess(orderHistories);
            }
        });
    }


    @Override
    public void run() {
        final OrderHistoryWrapper orderHistoryWrapper = mRepository.getOrderHistory(apiKey, userId);
        if(orderHistoryWrapper == null){
            notifyError("Something went wrong");
        }else if(!orderHistoryWrapper.status){
            notifyError(orderHistoryWrapper.message);
        }else{
            postMessage(orderHistoryWrapper.orderHistories);
        }
    }
}
