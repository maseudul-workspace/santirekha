package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetWalletStatusInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Wallet.WalletStatus;
import in.net.webinfotech.santirekha.domain.model.Wallet.WalletStatusWrapper;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 21-02-2019.
 */

public class GetWalletStatusInteractorImpl extends AbstractInteractor implements GetWalletStatusInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public GetWalletStatusInteractorImpl(Executor threadExecutor,
                                         MainThread mainThread,
                                         Callback callback,
                                         UserRepositoryImpl repository,
                                         String apiKey,
                                         int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletStatusFail(errorMsg);
            }
        });
    }

    private void postMessage(final WalletStatus walletStatus){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletStatusSuccess(walletStatus);
            }
        });
    }

    @Override
    public void run() {
        WalletStatusWrapper walletStatusWrapper = mRepository.getWalletStatus(apiKey, userId);
        if(walletStatusWrapper == null){
            notifyError("Something went wrong");
        }else if(!walletStatusWrapper.status){
            notifyError(walletStatusWrapper.message);
        }else{
            postMessage(walletStatusWrapper.walletStatus);
        }
    }
}
