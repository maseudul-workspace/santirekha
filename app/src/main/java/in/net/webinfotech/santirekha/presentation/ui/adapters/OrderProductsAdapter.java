package in.net.webinfotech.santirekha.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.Orders.OrderHistory;
import in.net.webinfotech.santirekha.domain.model.Orders.OrderProducts;
import in.net.webinfotech.santirekha.util.GlideHelper;

/**
 * Created by Raj on 22-02-2019.
 */

public class OrderProductsAdapter extends RecyclerView.Adapter<OrderProductsAdapter.ViewHolder>{

    Context mContext;
    OrderProducts[] orderProducts;

    public OrderProductsAdapter(Context mContext, OrderProducts[] orderProducts) {
        this.mContext = mContext;
        this.orderProducts = orderProducts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_products, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.productPoster, mContext.getResources().getString(R.string.base_url) + "uploads/product_image/thumb/" + orderProducts[position].product_image);
        holder.txtProductName.setText(orderProducts[position].product_name);
        holder.txtProductPrice.setText("Rs. " + orderProducts[position].order_price);
        holder.txtProductQty.setText(Integer.toString(orderProducts[position].order_quantity));
        Long total = orderProducts[position].order_price * orderProducts[position].order_quantity;
        holder.txtProductAmount.setText("Rs. " + total);

    }

    @Override
    public int getItemCount() {
        return orderProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        @BindView(R.id.txt_order_product_name)
        TextView txtProductName;
        @BindView(R.id.txt_order_product_amount)
        TextView txtProductAmount;
        @BindView(R.id.txt_order_product_price)
        TextView txtProductPrice;
        @BindView(R.id.txt_order_product_qty)
        TextView txtProductQty;
        @BindView(R.id.img_view_order_products_poster)
        ImageView productPoster;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = itemView;
        }
    }

}
