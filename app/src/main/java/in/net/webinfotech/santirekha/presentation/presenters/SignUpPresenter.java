package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 12-02-2019.
 */

public interface SignUpPresenter extends BasePresenter {
    void createUser(String name, String mobile,
                    String email, String password, String state,
                    String city, String address, String referalCode, String pin);
    interface View{
        void showProgressBar();
        void hideProgressBar();
    }
}
