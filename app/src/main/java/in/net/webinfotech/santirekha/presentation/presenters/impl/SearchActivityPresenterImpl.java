package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.santirekha.domain.interactors.SearchProductsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.AddToCartInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.SearchProductsInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.SearchActivityPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.routers.SearchActivityRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ProductListAdapter;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 27-02-2019.
 */

public class SearchActivityPresenterImpl extends AbstractPresenter implements SearchActivityPresenter,
                                                                                SearchProductsInteractor.Callback,
                                                                                ProductListAdapter.Callback,
        AddToCartInteractor.Callback{

    Context mContext;
    SearchActivityPresenter.View mView;
    SearchProductsInteractorImpl mInteractor;
    ProductListAdapter adapter;
    Product[] newProducts;
    String currentSearchKey;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    SearchActivityRouter mRouter;
    AddToCartInteractorImpl addToCartInteractor;

    public SearchActivityPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       SearchActivityPresenter.View view,
                                       SearchActivityRouter router
                                       ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
        mRouter = router;
    }

    @Override
    public void getProductList(String search_key, int page_no, String type) {
        currentSearchKey = search_key;
        if(type.equals("refresh")){
            newProducts = null;
        }
        mInteractor = new SearchProductsInteractorImpl(mExecutor, mMainThread, this, new GetProductsRepositoryImpl(), search_key, page_no);
        mInteractor.execute();
    }

    @Override
    public void onGettingProductsSuccess(Product[] products, String searchKey, int totalPage) {
        if(currentSearchKey.equals(searchKey)) {
            if(products.length == 0){
                adapter = new ProductListAdapter(mContext, this, products);
                mView.loadRecyclerViewAdapter(adapter, totalPage);
                mView.hideProgressBar();
                mView.stopRefreshing();
                mView.hidePaginationProgressBar();
                Toast.makeText(mContext, "No products found", Toast.LENGTH_SHORT).show();
            }else{
                Product[] tempProducts;
                tempProducts = newProducts;
                try {
                    int len1 = tempProducts.length;
                    int len2 = products.length;
                    newProducts = new Product[len1 + len2];
                    System.arraycopy(tempProducts, 0, newProducts, 0, len1);
                    System.arraycopy(products, 0, newProducts, len1, len2);
                    adapter.addItems(newProducts);
                    adapter.notifyDataSetChanged();
                    mView.hidePaginationProgressBar();
                } catch (NullPointerException e) {
                    newProducts = products;
                    adapter = new ProductListAdapter(mContext,this, products);
                    mView.loadRecyclerViewAdapter(adapter, totalPage);
                    mView.hideProgressBar();
                }
                mView.stopRefreshing();
            }
        }
    }


    @Override
    public void onGettingProductsFail(String searchKey) {
        newProducts = null;
        mView.hideProgressBar();
        mView.hidePaginationProgressBar();
        mView.stopRefreshing();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onClickOfImage(int id) {
        mRouter.goToProductDetailsActivity(id);
    }

    @Override
    public void onClickOfCart(int productId, int quantity) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext.getApplicationContext());
        if(userInfo == null){
            mView.showLoginSnackBar();
        }else {
            mView.showProgressBar();
            String apiKey = userInfo.apiKey;
            int userId = userInfo.userId;
            addToCartInteractor = new AddToCartInteractorImpl(mExecutor,
                    mMainThread,
                    new CartRepositoryImpl(),
                    this,
                    apiKey, productId,
                    userId, quantity
            );
            addToCartInteractor.execute();
        }
    }

    @Override
    public void onAddToCartSuccess(String successMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext,"Successfully added to cart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.hideProgressBar();
        if(errorMsg.equals("GoToCart")){
            Toast.makeText(mContext, "Already added to cart", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
