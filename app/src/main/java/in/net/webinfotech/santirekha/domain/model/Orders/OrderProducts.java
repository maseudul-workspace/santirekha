package in.net.webinfotech.santirekha.domain.model.Orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-02-2019.
 */

public class OrderProducts {
    @SerializedName("product_name")
    @Expose
    public String product_name;

    @SerializedName("product_image")
    @Expose
    public String product_image;

    @SerializedName("order_price")
    @Expose
    public Long order_price;

    @SerializedName("order_quantity")
    @Expose
    public int order_quantity;
}
