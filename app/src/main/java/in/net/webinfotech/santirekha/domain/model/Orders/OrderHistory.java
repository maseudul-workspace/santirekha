package in.net.webinfotech.santirekha.domain.model.Orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-02-2019.
 */

public class OrderHistory {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("total")
    @Expose
    public Long total;

    @SerializedName("discount")
    @Expose
    public Double discount;

    @SerializedName("payable_amount")
    @Expose
    public Double payable_amount;

    @SerializedName("wallet_pay")
    @Expose
    public Double wallet_pay;

    @SerializedName("delivery_status")
    @Expose
    public int delivery_status;

    @SerializedName("products")
    @Expose
    public OrderProducts[] products;

}
