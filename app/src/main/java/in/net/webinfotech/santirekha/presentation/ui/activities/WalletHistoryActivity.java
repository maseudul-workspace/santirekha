package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.presentation.presenters.WalletHistoryPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.WalletHistoryPresenterImpl;
import in.net.webinfotech.santirekha.presentation.ui.adapters.WalletHistoryAdapter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class WalletHistoryActivity extends AppCompatActivity implements WalletHistoryPresenter.View{

    WalletHistoryPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_wallet_history)
    RecyclerView recyclerViewWalletHistory;
    @BindView(R.id.txt_view_wallet_total_amount)
    TextView txtViewWalletTotalAmount;
    @BindView(R.id.txt_view_wallet_status)
    TextView txtViewWalletStatus;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_history);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.getWalletHistory();
        loadProgressBar();
    }

    public void initialisePresenter(){
        mPresenter = new WalletHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }


    @Override
    public void loadWalletAmount(double amount, int status) {
        txtViewWalletTotalAmount.setText(Double.toString(amount));
        if(status == 2){
            txtViewWalletStatus.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void loadAdapter(WalletHistoryAdapter adapter) {
        recyclerViewWalletHistory.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewWalletHistory.setAdapter(adapter);
        recyclerViewWalletHistory.setHasFixedSize(true);
    }

    @Override
    public void loadProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }
}
