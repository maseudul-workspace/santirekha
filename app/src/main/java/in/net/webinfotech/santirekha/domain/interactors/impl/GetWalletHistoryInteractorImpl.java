package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetWalletHistoryInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Wallet.WalletDetails;
import in.net.webinfotech.santirekha.domain.model.Wallet.WalletDetailsWrapper;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 22-02-2019.
 */

public class GetWalletHistoryInteractorImpl extends AbstractInteractor implements GetWalletHistoryInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public GetWalletHistoryInteractorImpl(Executor threadExecutor,
                                          MainThread mainThread,
                                          Callback callback,
                                          UserRepositoryImpl repository,
                                          String apiKey,
                                          int userId
                                          ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(final WalletDetails walletDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistorySuccess(walletDetails);
            }
        });
    }

    @Override
    public void run() {
        final WalletDetailsWrapper walletDetailsWrapper = mRepository.getWalletHistory(apiKey, userId);
        if(walletDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!walletDetailsWrapper.status){
            notifyError(walletDetailsWrapper.message);
        }else{
            postMessage(walletDetailsWrapper.walletDetails);
        }
    }
}
