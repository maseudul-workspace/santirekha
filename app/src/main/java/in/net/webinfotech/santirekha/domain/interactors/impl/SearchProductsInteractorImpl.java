package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.SearchProductsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.domain.model.Products.ProductSearchWrapper;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 27-02-2019.
 */

public class SearchProductsInteractorImpl extends AbstractInteractor implements SearchProductsInteractor {

    Callback mCallback;
    GetProductsRepositoryImpl mRepository;
    String searchKey;
    int pageNo;

    public SearchProductsInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        GetProductsRepositoryImpl repository,
                                        String searchKey,
                                        int pageNo
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        mRepository = repository;
        this.searchKey = searchKey;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductsFail(errorMsg);
            }
        });
    }

    private void postMessage(final Product[] products, final String searchKey, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductsSuccess(products, searchKey, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductSearchWrapper searchWrapper = mRepository.searchProdicts(pageNo, searchKey);
        if(searchWrapper == null){
            notifyError("");
        }else if(searchWrapper.products.length == 0 && !searchWrapper.status){
            postMessage(searchWrapper.products, searchWrapper.search_key, searchWrapper.totalPage);
        }else{
            postMessage(searchWrapper.products, searchWrapper.search_key, searchWrapper.totalPage);
        }
    }
}
