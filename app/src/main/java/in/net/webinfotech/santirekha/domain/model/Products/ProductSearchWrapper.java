package in.net.webinfotech.santirekha.domain.model.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 27-02-2019.
 */

public class ProductSearchWrapper {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("search_key")
    @Expose
    public String search_key;

    @SerializedName("data")
    @Expose
    public Product[] products;
}
