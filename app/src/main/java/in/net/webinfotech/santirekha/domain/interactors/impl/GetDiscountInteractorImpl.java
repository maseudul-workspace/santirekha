package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetDiscountInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Cart.Discount;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 26-02-2019.
 */

public class GetDiscountInteractorImpl extends AbstractInteractor implements GetDiscountInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public GetDiscountInteractorImpl(Executor threadExecutor,
                                     MainThread mainThread,
                                     Callback callback,
                                     UserRepositoryImpl repository,
                                     String apiKey,
                                     int userId
                                     ) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDiscountFail(errorMsg);
            }
        });
    }

    private void postMessage(final double discount){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDiscountSuccess(discount);
            }
        });
    }

    @Override
    public void run() {
        final Discount discountResponse = mRepository.getDiscount(apiKey, userId);
        if(discountResponse == null){
            notifyError("Something went wrong");
        }else if(!discountResponse.status){
            notifyError(discountResponse.message);
        }else{
            postMessage(discountResponse.discount);
        }
    }
}
