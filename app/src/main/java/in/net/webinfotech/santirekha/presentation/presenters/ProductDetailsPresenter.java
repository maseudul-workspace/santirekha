package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 07-01-2019.
 */

public interface ProductDetailsPresenter extends BasePresenter {
    void getProductDetails(int id);
    void addToCart(int productId, int quantity);
    interface View{
        void loadProductDetails(Product product);
        void showLoadingProgress();
        void hideLoadingProgress();
    }
}
