package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.domain.model.Category.Category;
import in.net.webinfotech.santirekha.domain.model.ImageSliders.ImageSlider;
import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ReviewsAdapter;

/**
 * Created by Raj on 10-01-2019.
 */

public interface MainActivityPresenter extends BasePresenter {
    void getCartCount();
    void getCategoriesList();
    void submitReview(int rating, String comments);
    void fetchReviews();
    void checkUserStatus();
    void getSliderImages();
    interface View{
        void loadCartCount(int count);
        void loadCategories(Category[] categories);
        void loadReviews(ReviewsAdapter adapter);
        void loadSliderImages(ImageSlider[] imageSliders);
    }
}
