package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.Products.Product;

/**
 * Created by Raj on 27-02-2019.
 */

public interface SearchProductsInteractor {
    interface Callback{
        void onGettingProductsSuccess(Product[] products, String searchKey, int totalPage);
        void onGettingProductsFail(String searchKey);
    }
}
