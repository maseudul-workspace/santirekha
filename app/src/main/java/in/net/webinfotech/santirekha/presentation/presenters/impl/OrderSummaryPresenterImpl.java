package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.AddShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.FetchShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetCartDetailsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetDiscountInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetWalletStatusInteractor;
import in.net.webinfotech.santirekha.domain.interactors.OrderPlaceInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.AddShippingAddressInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetCartDetailsInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetDiscountInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetWalletStatusInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.OrderPlaceInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Cart.Cart;
import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.domain.model.Wallet.WalletStatus;
import in.net.webinfotech.santirekha.presentation.presenters.OrderSummaryPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.routers.OrderSummaryRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ShippingAddressAdapter;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 21-02-2019.
 */

public class OrderSummaryPresenterImpl extends AbstractPresenter implements OrderSummaryPresenter,
                                                                            FetchShippingAddressInteractor.Callback,
                                                                            GetCartDetailsInteractor.Callback,
                                                                            GetWalletStatusInteractor.Callback,
                                                                            ShippingAddressAdapter.Callback,
                                                                            AddShippingAddressInteractor.Callback,
                                                                            GetDiscountInteractor.Callback,
                                                                            OrderPlaceInteractor.Callback
                                                                            {


    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    GetCartDetailsInteractorImpl getCartDetailsInteractor;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    Context mContext;
    OrderSummaryPresenter.View mView;
    ShippingAddressAdapter adapter;
    GetWalletStatusInteractorImpl walletStatusInteractor;
    AddShippingAddressInteractorImpl addShippingAddressInteractor;
    GetDiscountInteractorImpl discountInteractor;
    OrderPlaceInteractorImpl orderPlaceInteractor;
    OrderSummaryRouter mRouter;

    public OrderSummaryPresenterImpl(Executor executor,
                                     MainThread mainThread,
                                     Context context,
                                     OrderSummaryPresenter.View view,
                                     OrderSummaryRouter router) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
        this.mRouter = router;
    }

    @Override
    public void getShippingAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread,
                                                                        this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
        fetchShippingAddressInteractor.execute();
    }

    @Override
    public void onGettingShippingAddressSuccess(ShippingAddress[] shippingAddresses) {
        if(shippingAddresses.length > 0){
            adapter = new ShippingAddressAdapter(mContext, shippingAddresses, this, true);
            adapter.notifyDataSetChanged();
            mView.loadShippingAddress(shippingAddresses[0], true);
            mView.setShippingAddressDialog(adapter);
        }
    }

    @Override
    public void onGettingShippingAddressFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCartDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        getCartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor, mMainThread,
                                                                    new CartRepositoryImpl(), this, userInfo.userId, userInfo.apiKey);
        getCartDetailsInteractor.execute();
    }

    @Override
    public void onGetCartDetailsSuccess(Cart[] carts) {
        int cartCount = carts.length;
        int sum = 0;
        for(int i = 0; i < carts.length; i++){
            sum = sum + carts[i].quantity * carts[i].price;
        }
        mView.loadCartInfo(cartCount, sum);
    }

    @Override
    public void onGetCartDetailsFail(String errorMsg) {

    }


    @Override
    public void getWalletDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        walletStatusInteractor = new GetWalletStatusInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
        walletStatusInteractor.execute();
    }

    @Override
    public void onGettingWalletStatusSuccess(WalletStatus walletStatus) {
        if(walletStatus.status == 1){
            if(walletStatus.amount >= 0){
                mView.setWalletStatus(true, walletStatus.amount);
            }else{
                mView.setWalletStatus(false, 0);
            }
        }else {
            mView.setWalletStatus(false, 0);
        }
    }

    @Override
    public void onGettingWalletStatusFail(String errorMsg) {

    }

    @Override
    public void onEditClick(int position, int id) {

    }

    @Override
    public void onDeleteClick(int id) {

    }

    @Override
    public void onSetBtnClicked(ShippingAddress shippingAddress) {
       mView.loadShippingAddress(shippingAddress, true);
    }

    @Override
    public void addShippingAddress(String mobile, String email, String state, String city, String address, String pin) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        addShippingAddressInteractor = new AddShippingAddressInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(),
                userInfo.userId, userInfo.apiKey, mobile, email, state, city, address, pin);
        addShippingAddressInteractor.execute();
    }

    @Override
    public void onShippingAddressAddSuccess(String successMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext,"Shipping address added successfully", Toast.LENGTH_SHORT).show();
        getShippingAddress();
    }

    @Override
    public void onShippingAddressAddFail(String errorMsg) {
        Toast.makeText(mContext,errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getDiscount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        discountInteractor = new GetDiscountInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
        discountInteractor.execute();
    }

    @Override
    public void onGettingDiscountSuccess(double discount) {
        mView.setDiscount(discount);
    }

    @Override
    public void onGettingDiscountFail(String errorMsg) {
        mView.setDiscount(0);
    }

    @Override
    public void placeOrder(int walletStatus, int shippingAddressId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        orderPlaceInteractor = new OrderPlaceInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId, walletStatus, shippingAddressId);
        orderPlaceInteractor.execute();
    }

    @Override
    public void onOrderPlacedSuccess() {
        mView.hideProgressBar();
        Toast.makeText(mContext, "Your order is successfully placed", Toast.LENGTH_SHORT).show();
        mRouter.goToOrderHistory();
    }

    @Override
    public void onOrderPlacedFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
