package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.CheckUserInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.UserStatusCheckResponse;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 01-03-2019.
 */

public class CheckUserInteractorImpl extends AbstractInteractor implements CheckUserInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    int userId;

    public CheckUserInteractorImpl(Executor threadExecutor,
                                   MainThread mainThread,
                                   Callback callback,
                                   UserRepositoryImpl repository,
                                   int userId
                                   ) {
        super(threadExecutor, mainThread);
        this.userId = userId;
        this.mCallback = callback;
        this.mRepository = repository;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckingUserFail();
            }
        });
    }

    private void postMessage(final UserStatusCheckResponse statusCheckResponse){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckingUserSuccess(statusCheckResponse);
            }
        });
    }

    @Override
    public void run() {
        final UserStatusCheckResponse checkResponse = mRepository.checkUserStatus(userId);
        if(checkResponse == null){
            notifyError();
        }else{
            postMessage(checkResponse);
        }
    }
}
