package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.presentation.presenters.ProductListPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.ProductListPresenterImpl;
import in.net.webinfotech.santirekha.presentation.routers.ProductListActivityRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ProductListAdapter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class ProductListActivity extends AppCompatActivity implements ProductListPresenter.View, ProductListActivityRouter {

    ProductListPresenterImpl mPresenter;
    LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerViewProductList)
    RecyclerView recyclerView;
    int type;
    int categoryId;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.product_list_constraint_layout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.pagination_progressbar)
    View paginationLayout;
    AndroidApplication androidApplication;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        type = intent.getIntExtra("type", 0);
        categoryId = intent.getIntExtra("categoryId", 0);
        initialisePresenter();
    }

    public void initialisePresenter(){
        mPresenter = new ProductListPresenterImpl(ThreadExecutor.getInstance(),
                                                    MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void loadProductList(ProductListAdapter adapter, final int totalPageCount) {
        totalPage = totalPageCount;
        layoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        mPresenter.getProductList(type, pageNo, categoryId, "");
                    }
                }
            }
        });
    }

    @Override
    public void hideProgressList() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void showProgressList() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoginSnackbar() {
        Snackbar snackbar = Snackbar.make(constraintLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showPaginationProgressBar() {
        paginationLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationProgressBar() {
        paginationLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void goToProductDetailsActivity(int id) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("id",id);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        pageNo = 1;
        totalPage = 1;
        showProgressList();
        mPresenter.getProductList(type, 1, categoryId, "refresh");
    }




}
