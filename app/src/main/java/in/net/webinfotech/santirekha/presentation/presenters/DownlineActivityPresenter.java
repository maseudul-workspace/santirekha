package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.DownlineListAdapter;

/**
 * Created by Raj on 27-02-2019.
 */

public interface DownlineActivityPresenter extends BasePresenter {
    void getDownlineList();
    interface View{
        void showProgressBar();
        void hideProgressBar();
        void loadData(DownlineListAdapter adapter, int downlineCount);
    }
}
