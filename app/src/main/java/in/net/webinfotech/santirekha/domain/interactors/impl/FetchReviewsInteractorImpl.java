package in.net.webinfotech.santirekha.domain.interactors.impl;

import android.telecom.Call;
import android.util.Log;
import android.widget.RelativeLayout;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.FetchReviewsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Review.Review;
import in.net.webinfotech.santirekha.domain.model.Review.ReviewsResponseWrapper;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 28-02-2019.
 */

public class FetchReviewsInteractorImpl extends AbstractInteractor implements FetchReviewsInteractor {

    Callback mCallback;
    GetProductsRepositoryImpl mRepository;

    public FetchReviewsInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      Callback callback,
                                      GetProductsRepositoryImpl repository
                                      ) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        mCallback = callback;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingReviewsFail();
            }
        });
    }

    private void postMessage(final Review[] reviews){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingReviewsSuccess(reviews);
            }
        });
    }

    @Override
    public void run() {

        final ReviewsResponseWrapper responseWrapper = mRepository.fetchReviews();
        if(responseWrapper == null){
            notifyError();
        }else if(!responseWrapper.status){
            notifyError();
        }else{
            postMessage(responseWrapper.reviews);
        }
    }
}
