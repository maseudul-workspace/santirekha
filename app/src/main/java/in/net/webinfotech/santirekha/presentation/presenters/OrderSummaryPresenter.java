package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;
import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ShippingAddressAdapter;

/**
 * Created by Raj on 21-02-2019.
 */

public interface OrderSummaryPresenter extends BasePresenter{
    void getShippingAddress();
    void getCartDetails();
    void getWalletDetails();
    void getDiscount();
    void placeOrder(int walletStatus, int shippingAddressId);
    void addShippingAddress(String mobile, String email, String state, String city, String address, String pin);

        interface View{
        void loadShippingAddress(ShippingAddress shippingAddresses, boolean status);
        void loadCartInfo(int cartCount, int sum);
        void setWalletStatus(Boolean status, double amount);
        void setShippingAddressDialog(ShippingAddressAdapter adapter);
        void showProgressBar();
        void hideProgressBar();
        void setDiscount(double discount);
    }
}
