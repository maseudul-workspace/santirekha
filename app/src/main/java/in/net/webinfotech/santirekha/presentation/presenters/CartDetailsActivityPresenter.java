package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.domain.model.Cart.Cart;
import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.CartItemListAdapter;

/**
 * Created by Raj on 10-01-2019.
 */

public interface CartDetailsActivityPresenter extends BasePresenter {
    void getCartList();
    void updateCart(int cartId, int quantity);
    interface View{
        void loadCartItemList(CartItemListAdapter adapter, Cart[] carts);
        void loadCartSubtotal(int subtotal, int itemCount, boolean status);
        void showUpdateDialog(int position);
        void showLoadingProgress();
        void hideLoadingProgress();
    }
}
