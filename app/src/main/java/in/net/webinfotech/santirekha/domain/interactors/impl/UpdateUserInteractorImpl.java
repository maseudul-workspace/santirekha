package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.UpdateUserInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.UpdateUserResponse;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 14-02-2019.
 */

public class UpdateUserInteractorImpl extends AbstractInteractor implements UpdateUserInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;
    String name;
    String email;
    String state;
    String city;
    String address;
    String pin;

    public UpdateUserInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    Callback callback,
                                    UserRepositoryImpl repository,
                                    int userId,
                                    String apiKey,
                                    String name,
                                    String email,
                                    String state,
                                    String city,
                                    String address,
                                    String pin
                                    ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.userId = userId;
        this.apiKey = apiKey;
        this.name = name;
        this.email = email;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserUpdateFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserUpdateSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final UpdateUserResponse updateUserResponse = mRepository.updateUser(userId, apiKey, name, email, state, city, address, pin);
        if(updateUserResponse == null){
            notifyError("Something went wrong");
        }else if(!updateUserResponse.status){
            notifyError(updateUserResponse.message);
        }else{
            postMessage(updateUserResponse.message);
        }
    }
}
