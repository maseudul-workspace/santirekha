package in.net.webinfotech.santirekha.domain.interactors.impl;

import android.util.Log;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetProductListByCategoryInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.domain.model.Products.ProductListWrapper;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 05-01-2019.
 */

public class GetProductListByCategoryInteractorImpl extends AbstractInteractor implements GetProductListByCategoryInteractor {

    Callback mCallback;
    GetProductsRepositoryImpl mRepository;
    int type;
    int pageNo;
    int categoryId;

    public GetProductListByCategoryInteractorImpl(Executor threadExecutor,
                                                  MainThread mainThread,
                                                  Callback callback,
                                                  GetProductsRepositoryImpl mRepository,
                                                  int type,
                                                  int pageNo,
                                                  int categoryId) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = mRepository;
        this.type = type;
        this.pageNo = pageNo;
        this.categoryId = categoryId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(final Product[] productList, final int totalPages) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(productList, totalPages);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.getProductListByCategory(type, pageNo, categoryId);
        if(productListWrapper == null){
            notifyError("Something went wrong");
        }else if(!productListWrapper.status) {
            notifyError(productListWrapper.message);
        }else{
            postMessage(productListWrapper.products, productListWrapper.totalPage);
        }
    }
}
