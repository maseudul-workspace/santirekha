package in.net.webinfotech.santirekha.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.util.GlideHelper;

/**
 * Created by Raj on 28-02-2019.
 */

public class PackageListAdapter extends RecyclerView.Adapter<PackageListAdapter.ViewHolder>{

    public interface Callback{
        void goToPackageDetails(int id);
    }

    Context mContext;
    Product[] packages;
    Callback mCallback;

    public PackageListAdapter(Context mContext, Product[] packages, Callback callback) {
        this.mContext = mContext;
        this.packages = packages;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_package_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.imgPackagePoster, mContext.getResources().getString(R.string.base_url) + "uploads/product_image/thumb/" + packages[position].image);
        holder.txtViewPackageTitle.setText(packages[position].name);
        holder.txtViewPackageDescription.setText(packages[position].description);
        holder.imgPackagePoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.goToPackageDetails(packages[position].id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return packages.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        @BindView(R.id.txt_view_package_title)
        TextView txtViewPackageTitle;
        @BindView(R.id.txt_view_package_description)
        TextView txtViewPackageDescription;
        @BindView(R.id.img_view_package_poster)
        ImageView imgPackagePoster;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = itemView;
        }
    }

    public void updateData(Product[] products){
        this.packages = products;
    }

}
