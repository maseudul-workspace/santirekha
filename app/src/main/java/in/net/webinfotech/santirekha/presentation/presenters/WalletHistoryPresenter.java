package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.WalletHistoryAdapter;

/**
 * Created by Raj on 22-02-2019.
 */

public interface WalletHistoryPresenter extends BasePresenter{
    void getWalletHistory();
    interface View{
        void loadWalletAmount(double amount, int status);
        void loadAdapter(WalletHistoryAdapter adapter);
        void loadProgressBar();
        void hideProgressBar();
    }
}
