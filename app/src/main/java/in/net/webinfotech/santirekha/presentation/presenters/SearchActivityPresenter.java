package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ProductListAdapter;

/**
 * Created by Raj on 27-02-2019.
 */

public interface SearchActivityPresenter extends BasePresenter {

    interface View{
        void loadRecyclerViewAdapter(ProductListAdapter adapter, int totalPage);
        void showProgressBar();
        void hideProgressBar();
        void hidePaginationProgressBar();
        void showPaginationProgressBar();
        void stopRefreshing();
        void showLoginSnackBar();
    }
    void getProductList(String search_key, int page_no, String type);
}
