package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.PackageListAdapter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ProductListAdapter;

/**
 * Created by Raj on 28-02-2019.
 */

public interface PackageListActivityPresenter extends BasePresenter {
    void getProductList(int type, int pageNo, int categoryId, String state);
    interface View{
        void loadProductList(PackageListAdapter adapter, int totalPage);
        void hideProgressList();
        void showProgressList();
        void showPaginationProgressBar();
        void hidePaginationProgressBar();
    }
}
