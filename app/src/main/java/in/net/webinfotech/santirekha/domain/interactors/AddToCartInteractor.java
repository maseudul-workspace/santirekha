package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.Products.Product;

/**
 * Created by Raj on 09-01-2019.
 */

public interface AddToCartInteractor {
    interface Callback {
        void onAddToCartSuccess(String successMsg);
        void onAddToCartFail(String errorMsg);
    }
}
