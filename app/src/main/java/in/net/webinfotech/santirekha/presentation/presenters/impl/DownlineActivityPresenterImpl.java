package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.FetchDownlineInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.FetchDownlineListInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.User.Downline;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.DownlineActivityPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.DownlineListAdapter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 27-02-2019.
 */

public class DownlineActivityPresenterImpl extends AbstractPresenter implements DownlineActivityPresenter,
                                                                                FetchDownlineInteractor.Callback{

    Context mContext;
    FetchDownlineListInteractorImpl mInteractor;
    DownlineActivityPresenter.View mView;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    DownlineListAdapter adapter;

    public DownlineActivityPresenterImpl(Executor executor,
                                         MainThread mainThread,
                                         Context context,
                                         DownlineActivityPresenter.View view
                                         ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getDownlineList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchDownlineListInteractorImpl(mExecutor, mMainThread, this,
                                                            new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingDownlineListSuccess(Downline[] downlines) {
        adapter = new DownlineListAdapter(mContext, downlines);
        mView.loadData(adapter, downlines.length);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingDownlineListFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
