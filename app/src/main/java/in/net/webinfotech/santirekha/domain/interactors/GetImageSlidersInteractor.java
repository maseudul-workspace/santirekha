package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.ImageSliders.ImageSlider;

/**
 * Created by Raj on 24-06-2019.
 */

public interface GetImageSlidersInteractor {
    interface Callback{
        void onGettingImageSlidersSuccess(ImageSlider[] imageSliders);
        void ongettingImageSliderFail(String errorMsg);
    }
}
