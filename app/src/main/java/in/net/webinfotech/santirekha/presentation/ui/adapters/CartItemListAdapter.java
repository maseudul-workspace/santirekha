package in.net.webinfotech.santirekha.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.Cart.Cart;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.util.GlideHelper;

/**
 * Created by Raj on 10-01-2019.
 */

public class CartItemListAdapter extends RecyclerView.Adapter<CartItemListAdapter.ViewHolder> {

    Context mContext;
    Callback mCallback;
    Cart[] carts;

    public interface Callback{
        void onPreviewItemClicked(int position);
        void onDeleteItemClicked(int productId);
    }

    public CartItemListAdapter(Context context, Callback callback, Cart[] carts) {
        this.mContext = context;
        this.mCallback = callback;
        this.carts = carts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_cartitem_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.imgPoster, mContext.getResources().getString(R.string.base_url) + "uploads/product_image/thumb/" + carts[position].image);
        holder.txtName.setText(carts[position].name);
        int price = carts[position].price * carts[position].quantity;
        holder.txtPrice.setText("Rs. " + price);
        holder.txtQuantitiy.setText(Integer.toString(carts[position].quantity));
        holder.txtPreviewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onPreviewItemClicked(position);
            }
        });
        holder.txtDeleteCartItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCallback.onDeleteItemClicked(carts[position].cartId);
            }
        });

        if(carts[position].stock == 0){
            holder.txtCartStatus.setVisibility(View.VISIBLE);
            holder.txtCartStatus.setText("Out of Stock");
        }else if(carts[position].stock < carts[position].quantity){
            holder.txtCartStatus.setVisibility(View.VISIBLE);
            holder.txtCartStatus.setText("Qty is more");
        }

    }

    @Override
    public int getItemCount() {
        return carts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgCartItemPoster)
        ImageView imgPoster;
        @BindView(R.id.txtCartItemName)
        TextView txtName;
        @BindView(R.id.txtCartItemPrice)
        TextView txtPrice;
        @BindView(R.id.txtCartItemQuantity)
        TextView txtQuantitiy;
        @BindView(R.id.txtCartItemPreview)
        TextView txtPreviewItem;
        @BindView(R.id.txtCartItemDelete)
        TextView txtDeleteCartItem;
        @BindView(R.id.txt_view_cart_status)
        TextView txtCartStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
