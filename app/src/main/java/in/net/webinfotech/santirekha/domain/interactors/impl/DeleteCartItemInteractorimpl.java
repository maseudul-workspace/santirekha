package in.net.webinfotech.santirekha.domain.interactors.impl;

import android.util.Log;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.DeleteCartItemInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.Cart.CartDeleteResponse;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;

/**
 * Created by Raj on 10-01-2019.
 */

public class DeleteCartItemInteractorimpl extends AbstractInteractor implements DeleteCartItemInteractor {

    CartRepositoryImpl mRepository;
    int userId;
    int cartId;
    String apiKey;
    Callback mCallback;

    public DeleteCartItemInteractorimpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        CartRepositoryImpl repository,
                                        Callback callback,
                                        int userId,
                                        int cartId,
                                        String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.userId = userId;
        this.cartId = cartId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartItemDeleteFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartItemDeletedSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final CartDeleteResponse cartDeleteResponse = mRepository.deleteCartItem(userId, apiKey, cartId);
        if(cartDeleteResponse == null){
            notifyError("Something went wrong");
        }else if(!cartDeleteResponse.status){
            notifyError(cartDeleteResponse.message);
        } else{
            postMessage(cartDeleteResponse.message);
        }
    }
}
