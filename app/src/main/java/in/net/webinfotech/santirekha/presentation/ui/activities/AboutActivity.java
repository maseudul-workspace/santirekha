package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import in.net.webinfotech.santirekha.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
