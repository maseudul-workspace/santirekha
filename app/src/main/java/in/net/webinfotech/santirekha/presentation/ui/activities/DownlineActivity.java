package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.presentation.presenters.DownlineActivityPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.DownlineActivityPresenterImpl;
import in.net.webinfotech.santirekha.presentation.ui.adapters.DownlineListAdapter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class DownlineActivity extends AppCompatActivity implements DownlineActivityPresenter.View{

    @BindView(R.id.recycler_view_downline_list)
    RecyclerView recyclerView;
    @BindView(R.id.txt_view_downline_total)
    TextView txtViewDownlineTotal;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    DownlineActivityPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downline);
        ButterKnife.bind(this);
        intialisePresener();
        mPresenter.getDownlineList();
        showProgressBar();
    }

    public void intialisePresener(){
        mPresenter = new DownlineActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void loadData(DownlineListAdapter adapter, int downlineCount) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        txtViewDownlineTotal.setText(Integer.toString(downlineCount));
    }
}
