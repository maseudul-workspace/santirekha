package in.net.webinfotech.santirekha.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.User.Downline;

/**
 * Created by Raj on 27-02-2019.
 */

public class DownlineListAdapter extends RecyclerView.Adapter<DownlineListAdapter.ViewHolder> {

    Context mContext;
    Downline[] downlines;

    public DownlineListAdapter(Context mContext, Downline[] downlines) {
        this.mContext = mContext;
        this.downlines = downlines;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_downline, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtDownlineName.setText(downlines[position].name);
        holder.txtDownlineContactNo.setText("Contact No: " + downlines[position].mobile);
    }

    @Override
    public int getItemCount() {
        return downlines.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        @BindView(R.id.txt_view_downline_name)
        TextView txtDownlineName;
        @BindView(R.id.txt_downline_contact_no)
        TextView txtDownlineContactNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = itemView;
        }
    }

}
