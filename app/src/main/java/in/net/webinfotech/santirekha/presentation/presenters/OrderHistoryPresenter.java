package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.OrdersHistoryAdapter;

/**
 * Created by Raj on 22-02-2019.
 */

public interface OrderHistoryPresenter extends BasePresenter {
    void getOrderHistory();
    interface View{
        void loadOrdersAdapter(OrdersHistoryAdapter adapter);
        void showProgressBar();
        void hideProgressBar();
    }
}
