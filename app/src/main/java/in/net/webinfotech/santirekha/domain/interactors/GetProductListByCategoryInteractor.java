package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.interactors.base.Interactor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;

/**
 * Created by Raj on 05-01-2019.
 */

public interface GetProductListByCategoryInteractor extends Interactor{
    interface Callback{
        void onGettingProductListSuccess(Product[] productLists, int totalPages);
        void onGettingProductListFail(String errorMsg);
    }
}
