package in.net.webinfotech.santirekha.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;

/**
 * Created by Raj on 15-02-2019.
 */

public class ShippingAddressAdapter extends RecyclerView.Adapter<ShippingAddressAdapter.ViewHolder> {

    public interface Callback{
        void onEditClick(int position, int id);
        void onDeleteClick(int id);
        void onSetBtnClicked(ShippingAddress shippingAddress);
    }

    Context mContext;
    ShippingAddress[] shippingAddresses;
    Callback mCallback;
    Boolean isCheckout;

    public ShippingAddressAdapter(Context context, ShippingAddress[] shippingAddresses, Callback callback, Boolean isCheckout) {
        this.mContext = context;
        this.shippingAddresses = shippingAddresses;
        this.mCallback = callback;
        this.isCheckout = isCheckout;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_shipping_address, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if(isCheckout){
            holder.btnSetAddress.setVisibility(View.VISIBLE);
        }else{
            holder.btnLayout.setVisibility(View.VISIBLE);
        }
       holder.txtViewEmail.setText(shippingAddresses[position].email);
       holder.txtViewAddress.setText(shippingAddresses[position].address);
       holder.txtViewCity.setText(shippingAddresses[position].city);
       holder.txtViewPhone.setText(Long.toString(shippingAddresses[position].mobile));
       holder.txtViewPin.setText(Long.toString(shippingAddresses[position].pin));
       holder.txtViewState.setText(shippingAddresses[position].state);
       holder.txtViewShippingAddressEdit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mCallback.onEditClick(position, shippingAddresses[position].id);
           }
       });
       holder.txtViewShippingAddressDelete.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mCallback.onDeleteClick(shippingAddresses[position].id);
           }
       });
       holder.btnSetAddress.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mCallback.onSetBtnClicked(shippingAddresses[position]);
           }
       });
    }

    @Override
    public int getItemCount() {
        return shippingAddresses.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        @BindView(R.id.txt_view_email)
        TextView txtViewEmail;
        @BindView(R.id.txt_view_phone)
        TextView txtViewPhone;
        @BindView(R.id.txt_view_state)
        TextView txtViewState;
        @BindView(R.id.txt_view_city)
        TextView txtViewCity;
        @BindView(R.id.txt_view_pin)
        TextView txtViewPin;
        @BindView(R.id.txt_view_address)
        TextView txtViewAddress;
        @BindView(R.id.txt_view_shipping_address_edit)
        TextView txtViewShippingAddressEdit;
        @BindView(R.id.txt_view_shipping_address_delete)
        TextView txtViewShippingAddressDelete;
        @BindView(R.id.group_btns_layout)
        LinearLayout btnLayout;
        @BindView(R.id.btn_set_new_address)
        Button btnSetAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = itemView;
        }
    }

}
