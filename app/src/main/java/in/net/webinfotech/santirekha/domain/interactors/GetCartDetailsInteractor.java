package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.interactors.base.Interactor;
import in.net.webinfotech.santirekha.domain.model.Cart.Cart;
import in.net.webinfotech.santirekha.domain.model.Products.Product;

/**
 * Created by Raj on 10-01-2019.
 */

public interface GetCartDetailsInteractor extends Interactor {
    interface Callback{
        void onGetCartDetailsSuccess(Cart[] carts);
        void onGetCartDetailsFail(String errorMsg);
    }
}
