package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.ForgetPasswordInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.ForgotPasswordResponse;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 25-06-2019.
 */

public class ForgetPasswordInteractorImpl extends AbstractInteractor implements ForgetPasswordInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String email;

    public ForgetPasswordInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, UserRepositoryImpl mRepository, String email) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.email = email;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onForgetPasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onForgetPasswordSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final ForgotPasswordResponse forgotPasswordResponse = mRepository.forgotPassword(email);
        if(forgotPasswordResponse == null){
            notifyError("Something went wrong");
        }else if(!forgotPasswordResponse.status){
            notifyError(forgotPasswordResponse.message);
        }else{
            postMessage(forgotPasswordResponse.message);
        }
    }
}
