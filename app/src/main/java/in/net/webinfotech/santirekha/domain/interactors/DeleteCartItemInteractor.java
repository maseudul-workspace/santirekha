package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.interactors.base.Interactor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;

/**
 * Created by Raj on 10-01-2019.
 */

public interface DeleteCartItemInteractor extends Interactor {
    interface Callback{
        void onCartItemDeletedSuccess(String successMsg);
        void onCartItemDeleteFail(String errorMsg);
    }
}
