package in.net.webinfotech.santirekha.domain.interactors;

/**
 * Created by Raj on 14-02-2019.
 */

public interface UpdateUserInteractor {
    interface Callback{
        void onUserUpdateSuccess(String successMsg);
        void onUserUpdateFail(String errorMsg);
    }
}
