package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.FetchDownlineInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.Downline;
import in.net.webinfotech.santirekha.domain.model.User.DownlineWrapper;
import in.net.webinfotech.santirekha.domain.model.User.UserDetails;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 27-02-2019.
 */

public class FetchDownlineListInteractorImpl extends AbstractInteractor implements FetchDownlineInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public FetchDownlineListInteractorImpl(Executor threadExecutor,
                                           MainThread mainThread,
                                           Callback callback,
                                           UserRepositoryImpl repository,
                                           String apiKey,
                                           int userId
                                           ) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDownlineListFail(errorMsg);
            }
        });
    }

    private void postMessage(final Downline[] downlines){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDownlineListSuccess(downlines);
            }
        });
    }

    @Override
    public void run() {
        final DownlineWrapper downlineWrapper = mRepository.fetchDownlineList(apiKey, userId);
        if(downlineWrapper == null){
            notifyError("Something went wrong");
        }else if(!downlineWrapper.status){
            notifyError(downlineWrapper.message);
        }else{
            postMessage(downlineWrapper.downlines);
        }
    }
}
