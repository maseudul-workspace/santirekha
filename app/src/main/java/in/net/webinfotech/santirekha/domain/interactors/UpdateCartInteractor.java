package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.interactors.base.Interactor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;

/**
 * Created by Raj on 09-01-2019.
 */

public interface UpdateCartInteractor extends Interactor {
    interface Callback {
        void onUpdateCartSuccess(String successMsg);
        void onUpdateCartFail(String errorMsg);
    }
}
