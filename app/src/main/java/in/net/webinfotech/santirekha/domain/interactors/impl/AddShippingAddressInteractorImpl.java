package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.AddShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.ShippingAddressAddResponse;
import in.net.webinfotech.santirekha.repository.User.UserRepository;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 15-02-2019.
 */

public class AddShippingAddressInteractorImpl extends AbstractInteractor implements AddShippingAddressInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    int userId;
    String apiKey;
    String mobile;
    String email;
    String state;
    String city;
    String address;
    String pin;

    public AddShippingAddressInteractorImpl(Executor threadExecutor,
                                            MainThread mainThread,
                                            Callback callback,
                                            UserRepositoryImpl repository,
                                            int userId,
                                            String apiKey,
                                            String mobile,
                                            String email,
                                            String state,
                                            String city,
                                            String address,
                                            String pin
                                            ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.userId = userId;
        this.apiKey = apiKey;
        this.email = email;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pin = pin;
        this.mobile = mobile;
    }


    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onShippingAddressAddFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onShippingAddressAddSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final ShippingAddressAddResponse addressAddResponse = mRepository.addShippingAddress(userId, apiKey, mobile, email, state, city, address, pin);
        if(addressAddResponse == null){
            notifyError("Something went wrong");
        }else if(!addressAddResponse.status){
            notifyError(addressAddResponse.message);
        }else{
            postMessage(addressAddResponse.message);
        }

    }
}
