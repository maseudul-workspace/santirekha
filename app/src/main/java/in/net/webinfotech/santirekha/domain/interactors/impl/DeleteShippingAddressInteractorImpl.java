package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.DeleteShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.DeleteShippingAddressResponse;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 18-02-2019.
 */

public class DeleteShippingAddressInteractorImpl extends AbstractInteractor implements DeleteShippingAddressInteractor {

    Callback mCallback;
    UserRepositoryImpl userRepository;
    String apiKey;
    int userId;
    int addressId;

    public DeleteShippingAddressInteractorImpl(Executor threadExecutor,
                                               MainThread mainThread,
                                               Callback callback,
                                               UserRepositoryImpl repository,
                                               String apiKey,
                                               int userId,
                                               int addressId) {
        super(threadExecutor, mainThread);
        this.userRepository = repository;
        this.apiKey = apiKey;
        this.mCallback = callback;
        this.addressId = addressId;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteFail(errorMsg);
            }
        });
    }

    private void postMessage(final String succesMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteSucess(succesMsg);
            }
        });
    }

    @Override
    public void run() {
        final DeleteShippingAddressResponse deleteShippingAddressResponse = userRepository.deleteShippingAddress(userId, apiKey, addressId);
        if(deleteShippingAddressResponse == null){
            notifyError("Something went wrong");
        }else if(!deleteShippingAddressResponse.status){
            notifyError(deleteShippingAddressResponse.message);
        }else{
            postMessage(deleteShippingAddressResponse.message);
        }
    }
}
