package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.Review.Review;

/**
 * Created by Raj on 28-02-2019.
 */

public interface FetchReviewsInteractor {
    interface Callback{
        void onGettingReviewsSuccess(Review[] reviews);
        void onGettingReviewsFail();
    }
}
