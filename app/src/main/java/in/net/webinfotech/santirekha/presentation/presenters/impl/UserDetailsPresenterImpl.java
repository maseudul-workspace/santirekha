package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetUserDetailsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.UpdateUserInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetUserDetailsInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.UpdateUserInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.User.UserDetails;
import in.net.webinfotech.santirekha.presentation.presenters.UserDetailsPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 13-02-2019.
 */

public class UserDetailsPresenterImpl extends AbstractPresenter implements UserDetailsPresenter,
                                                                            GetUserDetailsInteractor.Callback,
                                                                            UpdateUserInteractor.Callback
                                                                            {

    Context mContext;
    UserDetailsPresenter.View mView;
    GetUserDetailsInteractorImpl mInteractor;
    UpdateUserInteractorImpl updateUserInteractor;

    public UserDetailsPresenterImpl(Executor executor,
                                    MainThread mainThread,
                                    Context context,
                                    UserDetailsPresenter.View view) {
        super(executor, mainThread);
        this.mView = view;
        this.mContext = context;
    }

    @Override
    public void fetchUserDetails(int userId, String apiKey) {
        mInteractor = new GetUserDetailsInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), apiKey, userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingUserDetailsSuccess(UserDetails userDetails) {
        mView.loadUserDetails(userDetails);
        mView.hideProgressBar();
        mView.stopRefreshing();
    }

    @Override
    public void onGettingUserDetailsFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.stopRefreshing();
    }

    @Override
    public void updateUser(int userId, String apiKey, String name, String email, String state, String city, String address, String pin) {
        updateUserInteractor = new UpdateUserInteractorImpl(mExecutor,
                                                            mMainThread, this,
                                                            new UserRepositoryImpl(), userId, apiKey,
                                                            name, email, state, city, address, pin);
        updateUserInteractor.execute();
    }

    @Override
    public void onUserUpdateSuccess(String successMsg) {
        Toast.makeText(mContext, successMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserUpdateFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
