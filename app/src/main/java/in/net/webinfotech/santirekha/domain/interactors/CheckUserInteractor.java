package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.User.UserStatusCheckResponse;

/**
 * Created by Raj on 01-03-2019.
 */

public interface CheckUserInteractor {
    interface Callback{
        void onCheckingUserFail();
        void onCheckingUserSuccess(UserStatusCheckResponse userStatusCheckResponse);
    }
}
