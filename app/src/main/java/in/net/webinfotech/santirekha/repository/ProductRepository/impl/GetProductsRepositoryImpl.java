package in.net.webinfotech.santirekha.repository.ProductRepository.impl;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.domain.model.Products.ProductDetailsWrapper;
import in.net.webinfotech.santirekha.domain.model.Products.ProductListWrapper;
import in.net.webinfotech.santirekha.domain.model.Products.ProductSearchWrapper;
import in.net.webinfotech.santirekha.domain.model.Review.ReviewsResponseWrapper;
import in.net.webinfotech.santirekha.repository.ApiClient;
import in.net.webinfotech.santirekha.repository.ProductRepository.GetProductsRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 05-01-2019.
 */

public class GetProductsRepositoryImpl {
    private GetProductsRepository mRepository;

    public GetProductsRepositoryImpl() {
        mRepository = ApiClient.createService(GetProductsRepository.class);
    }

    public ProductListWrapper getProductListByCategory(int type, int pageNo, int categoryId){
        ProductListWrapper productListWrapper = null;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> getMessages = mRepository.getProductListByCategory(type, pageNo, categoryId);
            Response<ResponseBody> response = getMessages.execute();
            if (response.body() != null) {
                responseBody = response.body().string();
            } else if (response.errorBody() != null) {
                responseBody = response.errorBody().string();
                isErrorResponse = true;
            }

            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse) {
                    productListWrapper = null;
                } else {
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }

        } catch (Exception e){
            productListWrapper = null;
        }
        return productListWrapper;
    }

    public ProductDetailsWrapper getProductDetailsById(int productId){
        ProductDetailsWrapper productDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> getDetails = mRepository.getProductDetailsById(productId);
            Response<ResponseBody> response = getDetails.execute();
            if (response.body() != null) {
                responseBody = response.body().string();
            } else if (response.errorBody() != null) {
                responseBody = response.errorBody().string();
                isErrorResponse = true;
            }

            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse) {
                    productDetailsWrapper = null;
                } else {
                    productDetailsWrapper = gson.fromJson(responseBody, ProductDetailsWrapper.class);
                }
            } else {
                productDetailsWrapper= null;
            }

        } catch (Exception e){
            productDetailsWrapper = null;
        }
        return productDetailsWrapper;
    }

    public ProductSearchWrapper searchProdicts(int pageNo, String searchKey){
        ProductSearchWrapper searchWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> search = mRepository.searchProducts(searchKey, pageNo);
            Response<ResponseBody> response = search.execute();
            if (response.body() != null) {
                responseBody = response.body().string();
            } else if (response.errorBody() != null) {
                responseBody = response.errorBody().string();
                isErrorResponse = true;
            }

            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse) {
                    searchWrapper = null;
                } else {
                    searchWrapper = gson.fromJson(responseBody, ProductSearchWrapper.class);
                }
            } else {
                searchWrapper = null;
            }

        } catch (Exception e){
            searchWrapper = null;
        }
        return searchWrapper;
    }

    public ReviewsResponseWrapper fetchReviews(){
        ReviewsResponseWrapper responseWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> fetch = mRepository.fetchReviews();
            Response<ResponseBody> response = fetch.execute();
            if (response.body() != null) {
                responseBody = response.body().string();
            } else if (response.errorBody() != null) {
                responseBody = response.errorBody().string();
                isErrorResponse = true;
            }

            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse) {
                    responseWrapper = null;
                } else {
                    responseWrapper = gson.fromJson(responseBody, ReviewsResponseWrapper.class);
                }
            } else {
                responseWrapper = null;
            }

        } catch (Exception e){

            responseWrapper = null;
        }
        return responseWrapper;
    }

}
