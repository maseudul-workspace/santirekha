package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 25-06-2019.
 */

public interface ForgetPasswordPresenter extends BasePresenter{
    void submitEmail(String email);
    interface View{
        void showLoader();
        void hideLoader();
    }
}
