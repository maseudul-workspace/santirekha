package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetOrderHistoryInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetOrderHistoryInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Orders.OrderHistory;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.OrderHistoryPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.OrdersHistoryAdapter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 22-02-2019.
 */

public class OrderHistoryPresenterImpl extends AbstractPresenter implements OrderHistoryPresenter,
                                                                            GetOrderHistoryInteractor.Callback{

    Context mContext;
    GetOrderHistoryInteractorImpl mInteractor;
    OrderHistoryPresenter.View mView;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    OrdersHistoryAdapter ordersHistoryAdapter;

    public OrderHistoryPresenterImpl(Executor executor,
                                     MainThread mainThread,
                                     Context context,
                                     OrderHistoryPresenter.View view
                                     ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getOrderHistory() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new GetOrderHistoryInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingOrderHistorySuccess(OrderHistory[] orderHistories) {
        ordersHistoryAdapter = new OrdersHistoryAdapter(mContext, orderHistories);
        mView.loadOrdersAdapter(ordersHistoryAdapter);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingOrderHistoryFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
