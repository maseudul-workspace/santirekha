package in.net.webinfotech.santirekha.presentation.routers;

/**
 * Created by Raj on 27-02-2019.
 */

public interface SearchActivityRouter {
    void goToProductDetailsActivity(int id);
}
