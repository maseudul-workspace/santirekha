package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;

/**
 * Created by Raj on 15-02-2019.
 */

public interface FetchShippingAddressInteractor {
    interface Callback{
        void onGettingShippingAddressSuccess(ShippingAddress[] shippingAddresses);
        void onGettingShippingAddressFail(String errorMsg);
    }
}
