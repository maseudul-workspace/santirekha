package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.OrderSummaryPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.OrderSummaryPresenterImpl;
import in.net.webinfotech.santirekha.presentation.routers.OrderSummaryRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ShippingAddressAdapter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class OrderSummaryActivity extends AppCompatActivity implements OrderSummaryPresenter.View, OrderSummaryRouter {

    OrderSummaryPresenterImpl mPresenter;
    @BindView(R.id.txt_view_email)
    TextView txtViewEmail;
    @BindView(R.id.txt_view_phone)
    TextView txtViewPhone;
    @BindView(R.id.txt_view_pin)
    TextView txtViewPin;
    @BindView(R.id.txt_view_city)
    TextView txtViewCity;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    @BindView(R.id.txt_view_address)
    TextView txtViewAddress;
    AlertDialog addressDialog;
    AlertDialog addAddressDialog;
    @BindView(R.id.linear_layout_wallet)
    LinearLayout walletLayout;
    @BindView(R.id.txt_total_amount_summary)
    TextView txtTotalAmount;
    @BindView(R.id.txt_total_items_summary)
    TextView txtTotalItems;
    @BindView(R.id.txt_net_amount_summary)
    TextView txtNetAmountSummary;
    @BindView(R.id.txt_discount_summary)
    TextView txtDiscount;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    @BindView(R.id.walletCheckbox)
    CheckBox walletCheckBox;
    @BindView(R.id.txt_wallet_summary)
    TextView txtWalletSummary;
    @BindView(R.id.txt_view_wallet_amount)
    TextView txtWalletAmount;
    Double walletAmount;
    int totalAmount;
    double netAmount;
    int walletStatus = 1;
    int shippingAddressId;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    boolean isShippingAddressAdded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.getCartDetails();
        mPresenter.getShippingAddress();
        mPresenter.getWalletDetails();
        mPresenter.getDiscount();
        walletCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    walletStatus = 2;
                    txtWalletSummary.setVisibility(View.VISIBLE);
                    if(walletAmount >= netAmount){
                        txtWalletSummary.setText("Wallet Pay: Rs. " + netAmount);
                        double netWalletAmt = walletAmount - netAmount;
                        txtWalletAmount.setText("Wallet Amount: Rs. " + netWalletAmt);
                        txtNetAmountSummary.setText("Net Amount: Rs. 0");
                    }else{
                        txtWalletSummary.setText("Wallet Pay: Rs. " + walletAmount);
                        txtWalletAmount.setText("Wallet Amount: Rs. 0.0");
                        double amount = netAmount - walletAmount;
                        txtNetAmountSummary.setText("Net Amount: Rs. " + amount);
                    }
                }else{
                    walletStatus = 1;
                    txtWalletSummary.setVisibility(View.GONE);
                    txtNetAmountSummary.setText("Net Amount: Rs. " + netAmount);
                    txtWalletAmount.setText("Wallet Amount: Rs. " + walletAmount);
                }
            }
        });
    }

    public void initialisePresenter(){
        mPresenter = new OrderSummaryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void loadShippingAddress(ShippingAddress shippingAddresses, boolean status) {
        isShippingAddressAdded = status;
        txtViewEmail.setText(shippingAddresses.email);
        txtViewPhone.setText(Long.toString(shippingAddresses.mobile));
        txtViewState.setText(shippingAddresses.state);
        txtViewPin.setText(Long.toString(shippingAddresses.pin));
        txtViewCity.setText(shippingAddresses.city);
        txtViewAddress.setText(shippingAddresses.address);
        shippingAddressId = shippingAddresses.id;
        try{
            addressDialog.dismiss();
        }catch (NullPointerException e){

        }
    }

    @Override
    public void setShippingAddressDialog(ShippingAddressAdapter adapter){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.select_address_dialog_layout, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_dialog_address);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        builder.setView(view);
        addressDialog = builder.create();
    }

    @OnClick(R.id.btn_change_address) void showAddressDialog(){
        try{
            addressDialog.show();
        }catch (NullPointerException e){
            Toast.makeText(this, "No shipping address found !!! Please add", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_add_new_address) void addNewAddress(){
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.shipping_address_form_layout, null);
        final EditText editTextEmail = (EditText) view.findViewById(R.id.edit_text_email);
        final EditText editTextPhone = (EditText) view.findViewById(R.id.edit_text_phone);
        final EditText editTextState = (EditText) view.findViewById(R.id.edit_text_state);
        final EditText editTextCity = (EditText) view.findViewById(R.id.edit_text_city);
        final EditText editTextPin = (EditText) view.findViewById(R.id.edit_text_pin);
        final EditText editTextAddress = (EditText) view.findViewById(R.id.edit_text_address);
        editTextEmail.setText(userInfo.email);
        editTextPhone.setText(Long.toString(userInfo.mobile));
        Button submitBtn  =(Button) view.findViewById(R.id.btn_submit_shipping_address);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextEmail.getText().toString().trim().isEmpty() ||
                        editTextPhone.getText().toString().trim().isEmpty() ||
                        editTextState.getText().toString().trim().isEmpty() ||
                        editTextCity.getText().toString().trim().isEmpty() ||
                        editTextPin.getText().toString().trim().isEmpty() ||
                        editTextAddress.getText().toString().trim().isEmpty()
                        )
                {
                    Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_SHORT).show();
                }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
                    Toast.makeText(getApplicationContext(), "Please enter a valid email",Toast.LENGTH_SHORT).show();
                }else{
                    mPresenter.addShippingAddress(editTextPhone.getText().toString(),
                            editTextEmail.getText().toString(), editTextState.getText().toString(),
                            editTextCity.getText().toString(), editTextAddress.getText().toString(), editTextPin.getText().toString());
                    addAddressDialog.dismiss();
                    showProgressBar();
                }
            }
        });
        builder.setView(view);
        addAddressDialog = builder.create();
        addAddressDialog.show();
    }

    @Override
    public void loadCartInfo(int cartCount, int sum) {
        txtTotalAmount.setText("Total Amount: Rs. " + Integer.toString(sum));
        txtNetAmountSummary.setText("Net Amount: Rs. " + Integer.toString(sum));
        txtTotalItems.setText("Total Items: " + Integer.toString(cartCount));
        totalAmount = sum;
        netAmount = sum;
    }

    @Override
    public void setWalletStatus(Boolean status, double amount) {
        if(status){
            walletAmount = amount;
            walletLayout.setVisibility(View.VISIBLE);
            txtWalletAmount.setText("Wallet Amount: Rs. " + amount);
        }else {
            walletLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void setDiscount(double discount) {
        if(discount > 0){
            if(totalAmount >= 3000){
                double discountAmt = totalAmount * discount/100;
                txtDiscount.setVisibility(View.VISIBLE);
                txtDiscount.setText("Discount: Rs. " + discountAmt);
                netAmount = totalAmount - discountAmt;
                txtNetAmountSummary.setText("Net Amount: Rs. " + netAmount);
            }
        }
    }

    @OnClick(R.id.btn_checkout) void onCheckoutBtnClicked(){
        if(!isShippingAddressAdded){
            Toast.makeText(this, "Please add shipping address", Toast.LENGTH_SHORT).show();
        }else{
            mPresenter.placeOrder(walletStatus, shippingAddressId);
            showProgressBar();
        }
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToOrderHistory() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
        finish();
    }
}
