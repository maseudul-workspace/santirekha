package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.interactors.base.Interactor;
import in.net.webinfotech.santirekha.domain.model.Products.Product;

/**
 * Created by Raj on 07-01-2019.
 */

public interface GetProductDetailsByIdInteractor extends Interactor{
    interface Callback{
        void onGettingProductDetailsSuccess(Product productLists);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
