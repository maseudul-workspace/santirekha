package in.net.webinfotech.santirekha.domain.interactors;

import in.net.webinfotech.santirekha.domain.model.Wallet.WalletStatus;

/**
 * Created by Raj on 21-02-2019.
 */

public interface GetWalletStatusInteractor {
    interface Callback{
        void onGettingWalletStatusSuccess(WalletStatus walletStatus);
        void onGettingWalletStatusFail(String errorMsg);
    }
}
