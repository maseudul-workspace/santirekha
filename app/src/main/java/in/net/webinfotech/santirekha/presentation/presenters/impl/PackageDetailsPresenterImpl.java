package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetProductDetailsByIdInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetProductDetailsByIdInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.presentation.presenters.PackageDetailsPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 28-02-2019.
 */

public class PackageDetailsPresenterImpl extends AbstractPresenter implements PackageDetailsPresenter,
                                                                                GetProductDetailsByIdInteractor.Callback{

    Context mContext;
    PackageDetailsPresenter.View mView;
    GetProductDetailsByIdInteractorImpl mInteractor;

    public PackageDetailsPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       PackageDetailsPresenter.View view
                                       ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
    }

    @Override
    public void getProductDetails(int id) {
        mInteractor = new GetProductDetailsByIdInteractorImpl(mExecutor,
                mMainThread,
                this,
                new GetProductsRepositoryImpl(),
                id);
        mInteractor.execute();
    }

    @Override
    public void onGettingProductDetailsSuccess(Product productLists) {
        mView.loadProductDetails(productLists);
        mView.hideLoadingProgress();
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideLoadingProgress();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
