package in.net.webinfotech.santirekha.presentation.ui.activities;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.ShippingAddressPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.impl.ShippingAddressPresenterImpl;
import in.net.webinfotech.santirekha.presentation.ui.adapters.ShippingAddressAdapter;
import in.net.webinfotech.santirekha.threading.MainThreadImpl;

public class ShippingAddressActivity extends AppCompatActivity implements ShippingAddressPresenter.View {

    @BindView(R.id.recycler_view_shipping_address)
    RecyclerView recyclerView;
    ShippingAddress[] shippingAddresses;
    ShippingAddressPresenterImpl mPresenter;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    AlertDialog dialog;
    @BindView(R.id.progressbar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_address);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        initalisePresenter();
        showProgresBar();
        mPresenter.getShippingAddress(userInfo.userId, userInfo.apiKey);
    }

    public void initalisePresenter(){
        mPresenter = new ShippingAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_add_shipping_address) void showFormDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.shipping_address_form_layout, null);
        final EditText editTextEmail = (EditText) view.findViewById(R.id.edit_text_email);
        final EditText editTextPhone = (EditText) view.findViewById(R.id.edit_text_phone);
        final EditText editTextState = (EditText) view.findViewById(R.id.edit_text_state);
        final EditText editTextCity = (EditText) view.findViewById(R.id.edit_text_city);
        final EditText editTextPin = (EditText) view.findViewById(R.id.edit_text_pin);
        final EditText editTextAddress = (EditText) view.findViewById(R.id.edit_text_address);
        editTextEmail.setText(userInfo.email);
        editTextPhone.setText(Long.toString(userInfo.mobile));
        Button submitBtn  =(Button) view.findViewById(R.id.btn_submit_shipping_address);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextEmail.getText().toString().trim().isEmpty() ||
                        editTextPhone.getText().toString().trim().isEmpty() ||
                        editTextState.getText().toString().trim().isEmpty() ||
                        editTextCity.getText().toString().trim().isEmpty() ||
                        editTextPin.getText().toString().trim().isEmpty() ||
                        editTextAddress.getText().toString().trim().isEmpty()
                        )
                {
                    Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_SHORT).show();
                }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
                    Toast.makeText(getApplicationContext(), "Please enter a valid email",Toast.LENGTH_SHORT).show();
                }else{
                    mPresenter.addShippingAddress(userInfo.userId, userInfo.apiKey, editTextPhone.getText().toString(),
                                                    editTextEmail.getText().toString(), editTextState.getText().toString(),
                                                    editTextCity.getText().toString(), editTextAddress.getText().toString(), editTextPin.getText().toString());
                    dialog.dismiss();
                    showProgresBar();
                }
            }
        });
        builder.setView(view);
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showProgresBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void setRecyclerViewAdapter(ShippingAddressAdapter adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void loadEditDialog(final ShippingAddress shippingAddress) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.shipping_address_form_layout, null);
        final EditText editTextEmail = (EditText) view.findViewById(R.id.edit_text_email);
        final EditText editTextPhone = (EditText) view.findViewById(R.id.edit_text_phone);
        final EditText editTextState = (EditText) view.findViewById(R.id.edit_text_state);
        final EditText editTextCity = (EditText) view.findViewById(R.id.edit_text_city);
        final EditText editTextPin = (EditText) view.findViewById(R.id.edit_text_pin);
        final EditText editTextAddress = (EditText) view.findViewById(R.id.edit_text_address);
        editTextEmail.setText(shippingAddress.email);
        editTextPhone.setText(Long.toString(shippingAddress.mobile));
        editTextState.setText(shippingAddress.state);
        editTextAddress.setText(shippingAddress.address);
        editTextCity.setText(shippingAddress.city);
        editTextPin.setText(Long.toString(shippingAddress.pin));
        Button submitBtn  =(Button) view.findViewById(R.id.btn_submit_shipping_address);
        submitBtn.setText("Update");
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextEmail.getText().toString().trim().isEmpty() ||
                        editTextPhone.getText().toString().trim().isEmpty() ||
                        editTextState.getText().toString().trim().isEmpty() ||
                        editTextCity.getText().toString().trim().isEmpty() ||
                        editTextPin.getText().toString().trim().isEmpty() ||
                        editTextAddress.getText().toString().trim().isEmpty()
                        )
                {
                    Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_SHORT).show();
                }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
                    Toast.makeText(getApplicationContext(), "Please enter a valid email",Toast.LENGTH_SHORT).show();
                }else{
                    mPresenter.updateAddress(userInfo.userId, userInfo.apiKey,
                            shippingAddress.id,  editTextPhone.getText().toString(),
                            editTextEmail.getText().toString(), editTextState.getText().toString(),
                            editTextCity.getText().toString(), editTextAddress.getText().toString(), editTextPin.getText().toString());
                    dialog.dismiss();
                    showProgresBar();

                }
            }
        });
        builder.setView(view);
        dialog = builder.create();
        dialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
