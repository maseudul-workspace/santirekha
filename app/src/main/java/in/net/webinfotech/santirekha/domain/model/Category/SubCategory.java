package in.net.webinfotech.santirekha.domain.model.Category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 11-02-2019.
 */

public class SubCategory {

    @SerializedName("sub_cat_id")
    @Expose
    public int subCatId;

    @SerializedName("sub_cat_name")
    @Expose
    public String subCatName;

}
