package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.domain.model.User.UserDetails;
import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 13-02-2019.
 */

public interface UserDetailsPresenter extends BasePresenter {
    void fetchUserDetails(int userId, String apiKey);
    void updateUser(int userId,
                    String apiKey,
                    String name,
                    String email,
                    String state,
                    String city,
                    String address,
                    String pin);
    interface View{
        void loadUserDetails(UserDetails userDetails);
        void showProgressBar();
        void hideProgressBar();
        void stopRefreshing();
    }
}
