package in.net.webinfotech.santirekha.repository.Category;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 11-02-2019.
 */

public interface CategoryRepository {
    @POST("api/category/category_list.php")
    Call<ResponseBody> getCategoryList();

    @POST("api/slider/sliders.php")
    Call<ResponseBody> getImageSliders();

}
