package in.net.webinfotech.santirekha.domain.interactors.impl;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.FetchShippingAddressInteractor;
import in.net.webinfotech.santirekha.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.santirekha.domain.model.User.ShippingAddress;
import in.net.webinfotech.santirekha.domain.model.User.ShippingAddressWrapper;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 15-02-2019.
 */

public class FetchShippingAddressInteractorImpl extends AbstractInteractor implements FetchShippingAddressInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String apiKey;
    int userId;

    public FetchShippingAddressInteractorImpl(Executor threadExecutor,
                                              MainThread mainThread,
                                              Callback callback,
                                              UserRepositoryImpl repository,
                                              String apiKey,
                                              int userId
                                              ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingShippingAddressFail(errorMsg);
            }
        });
    }

    private void postMessage(final ShippingAddress[] shippingAddresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingShippingAddressSuccess(shippingAddresses);
            }
        });
    }

    @Override
    public void run() {
        final ShippingAddressWrapper shippingAddressWrapper = mRepository.fetchShippingAddress(userId, apiKey);
        if(shippingAddressWrapper == null){
            notifyError("Something went wrong");
        }else if(!shippingAddressWrapper.status){
            notifyError(shippingAddressWrapper.message);
        }else{
            postMessage(shippingAddressWrapper.shippingAddresses);
        }
    }
}
