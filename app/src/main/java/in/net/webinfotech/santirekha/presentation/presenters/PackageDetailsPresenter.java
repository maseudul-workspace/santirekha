package in.net.webinfotech.santirekha.presentation.presenters;

import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 28-02-2019.
 */

public interface PackageDetailsPresenter extends BasePresenter {
    void getProductDetails(int id);
    interface View{
        void loadProductDetails(Product product);
        void showLoadingProgress();
        void hideLoadingProgress();
    }
}
