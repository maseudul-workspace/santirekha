package in.net.webinfotech.santirekha.domain.model.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 12-02-2019.
 */

public class UserInfo {
    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("api_key")
    @Expose
    public String apiKey;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public Long mobile;


}
