package in.net.webinfotech.santirekha.domain.interactors;

/**
 * Created by Raj on 26-02-2019.
 */

public interface GetDiscountInteractor {
    interface Callback{
        void onGettingDiscountSuccess(double discount);
        void onGettingDiscountFail(String errorMsg);
    }
}
