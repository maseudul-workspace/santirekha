package in.net.webinfotech.santirekha.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.Wallet.WalletHistory;

/**
 * Created by Raj on 22-02-2019.
 */

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder> {

    Context mContext;
    WalletHistory[] walletHistories;

    public WalletHistoryAdapter(Context context, WalletHistory[] walletHistories) {
        mContext = context;
        this.walletHistories = walletHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_wallet_history, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtWalletComment.setText(walletHistories[position].comment);
        holder.txtWalletAmount.setText(Double.toString(walletHistories[position].wallet_amount));
        holder.txtWalletTransaction.setText(Double.toString(walletHistories[position].transaction_amount));
        String dateString = walletHistories[position].date + " " + walletHistories[position].time;
        if(walletHistories[position].transaction_type == 1){
            holder.txtWalletStatus.setText("debit");
            holder.txtWalletStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
        }else{
            holder.txtWalletStatus.setText("credit");
            holder.txtWalletStatus.setTextColor(mContext.getResources().getColor(R.color.green2));
        }

        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
        try {
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            holder.txtWalletDate.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return walletHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        @BindView(R.id.txt_view_wallet_comment)
        TextView txtWalletComment;
        @BindView(R.id.txt_view_wallet_status)
        TextView txtWalletStatus;
        @BindView(R.id.txt_view_wallet_amount)
        TextView txtWalletAmount;
        @BindView(R.id.txt_view_wallet_transaction)
        TextView txtWalletTransaction;
        @BindView(R.id.txt_view_wallet_date)
        TextView txtWalletDate;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = itemView;
        }
    }

}
