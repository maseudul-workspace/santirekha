package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.SignUpInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.SignUpInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.SignUpPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.routers.SignUpRouter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 12-02-2019.
 */

public class SignUpPresenterImpl extends AbstractPresenter implements SignUpPresenter, SignUpInteractor.Callback {

    Context mContext;
    SignUpPresenter.View mView;
    SignUpInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    SignUpRouter mRouter;

    public SignUpPresenterImpl(Executor executor,
                               MainThread mainThread,
                               Context context,
                               SignUpPresenter.View view,
                               SignUpRouter router) {
        super(executor, mainThread);
        mView = view;
        mContext = context;
        mRouter = router;
    }

    @Override
    public void createUser(String name, String mobile, String email, String password, String state, String city, String address, String referalCode, String pin) {
        mInteractor = new SignUpInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(),
                                                name, mobile, email, password, state, city, address, referalCode, pin);
        mInteractor.execute();
    }

    @Override
    public void onSignUpSuccess(UserInfo userInfo) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.hideProgressBar();
        Toast.makeText(mContext, "Registration Successful", Toast.LENGTH_SHORT).show();
        this.mRouter.goToMain();
    }

    @Override
    public void onSignUpFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
