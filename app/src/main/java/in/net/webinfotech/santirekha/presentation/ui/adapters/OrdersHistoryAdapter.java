package in.net.webinfotech.santirekha.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.santirekha.R;
import in.net.webinfotech.santirekha.domain.model.Orders.OrderHistory;

/**
 * Created by Raj on 25-02-2019.
 */

public class OrdersHistoryAdapter extends RecyclerView.Adapter<OrdersHistoryAdapter.ViewHolder>{

    Context mContext;
    OrderHistory[] orderHistories;

    public OrdersHistoryAdapter(Context mContext, OrderHistory[] orderHistories) {
        this.mContext = mContext;
        this.orderHistories = orderHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_ids, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderProductsAdapter orderProductsAdapter = new OrderProductsAdapter(mContext, orderHistories[position].products);
        holder.recyclerViewOrderProducts.setAdapter(orderProductsAdapter);
        holder.recyclerViewOrderProducts.setLayoutManager(new LinearLayoutManager(mContext));
        holder.recyclerViewOrderProducts.setHasFixedSize(true);
        holder.txtOrderId.setText("Order Id: " + orderHistories[position].id);
        holder.txtTotal.setText("Total Amount: Rs. " + orderHistories[position].total);
        holder.txtNet.setText("Net Amount: Rs. " + orderHistories[position].payable_amount);
        if(orderHistories[position].discount > 0){
            holder.txtDiscount.setVisibility(View.VISIBLE);
            holder.txtDiscount.setText("Discount: Rs. " + orderHistories[position].discount);
        }
        if(orderHistories[position].wallet_pay > 0){
            holder.txtWallet.setVisibility(View.VISIBLE);
            holder.txtWallet.setText("Wallet Pay: Rs. " + orderHistories[position].wallet_pay);
        }
        if(orderHistories[position].delivery_status == 1){
            holder.txtOrderStatus.setText("Status: pending");
        }else{
            holder.txtOrderStatus.setText("Status: delivered");
        }
    }

    @Override
    public int getItemCount() {
        return orderHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        @BindView(R.id.recycler_view_order_products)
        RecyclerView recyclerViewOrderProducts;
        @BindView(R.id.txt_view_order_history_discount)
        TextView txtDiscount;
        @BindView(R.id.txt_view_order_history_total)
        TextView txtTotal;
        @BindView(R.id.txt_view_order_history_wallet)
        TextView txtWallet;
        @BindView(R.id.txt_view_order_history_net)
        TextView txtNet;
        @BindView(R.id.txt_order_id)
        TextView txtOrderId;
        @BindView(R.id.txt_view_order_history_status)
        TextView txtOrderStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = itemView;
        }
    }

}
