package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.GetWalletHistoryInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetWalletHistoryInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.domain.model.Wallet.WalletDetails;
import in.net.webinfotech.santirekha.presentation.presenters.WalletHistoryPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.WalletHistoryAdapter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 22-02-2019.
 */

public class WalletHistoryPresenterImpl extends AbstractPresenter implements WalletHistoryPresenter,
                                                                                GetWalletHistoryInteractor.Callback{


    GetWalletHistoryInteractorImpl mInteractor;
    WalletHistoryPresenter.View mView;
    Context mContext;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    WalletHistoryAdapter adapter;

    public WalletHistoryPresenterImpl(Executor executor,
                                      MainThread mainThread,
                                      Context context,
                                      WalletHistoryPresenter.View view
                                      ) {
        super(executor, mainThread);
        mView = view;
        mContext = context;
    }

    @Override
    public void getWalletHistory() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new GetWalletHistoryInteractorImpl(mExecutor, mMainThread,
                                                        this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
        mInteractor.execute();
    }

    @Override
    public void onGettingWalletHistorySuccess(WalletDetails walletDetails) {
        adapter = new WalletHistoryAdapter(mContext, walletDetails.walletHistories);
        mView.loadAdapter(adapter);
        mView.loadWalletAmount(walletDetails.amount, walletDetails.status);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingWalletHistoryFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideProgressBar();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

}
