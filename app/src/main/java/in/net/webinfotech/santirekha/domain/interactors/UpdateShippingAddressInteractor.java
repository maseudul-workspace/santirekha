package in.net.webinfotech.santirekha.domain.interactors;

/**
 * Created by Raj on 15-02-2019.
 */

public interface UpdateShippingAddressInteractor {
    interface Callback{
        void onUpdateAddressSuccess(String successMsg);
        void onUpdateAddressFail(String errorMsg);
    }
}
