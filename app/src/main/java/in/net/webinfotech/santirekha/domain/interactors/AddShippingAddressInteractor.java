package in.net.webinfotech.santirekha.domain.interactors;

/**
 * Created by Raj on 15-02-2019.
 */

public interface AddShippingAddressInteractor {
    interface Callback{
        void onShippingAddressAddSuccess(String successMsg);
        void onShippingAddressAddFail(String errorMsg);
    }
}
