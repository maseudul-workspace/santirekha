package in.net.webinfotech.santirekha.domain.model.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 13-02-2019.
 */

public class PermanentAddress {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("location")
    @Expose
    public String location;

    @SerializedName("pin")
    @Expose
    public long pin;

}
