package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.ForgetPasswordInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.ForgetPasswordInteractorImpl;
import in.net.webinfotech.santirekha.presentation.presenters.ForgetPasswordPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 25-06-2019.
 */

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter, ForgetPasswordInteractor.Callback {

    Context mContext;
    ForgetPasswordPresenter.View mView;
    ForgetPasswordInteractorImpl mInteractor;

    public ForgetPasswordPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       ForgetPasswordPresenter.View view
                                       ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void submitEmail(String email) {
        mInteractor = new ForgetPasswordInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), email);
        mInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onForgetPasswordSuccess(String successMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, successMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onForgetPasswordFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
