package in.net.webinfotech.santirekha.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import in.net.webinfotech.santirekha.AndroidApplication;
import in.net.webinfotech.santirekha.domain.executors.Executor;
import in.net.webinfotech.santirekha.domain.executors.MainThread;
import in.net.webinfotech.santirekha.domain.interactors.DeleteCartItemInteractor;
import in.net.webinfotech.santirekha.domain.interactors.GetCartDetailsInteractor;
import in.net.webinfotech.santirekha.domain.interactors.UpdateCartInteractor;
import in.net.webinfotech.santirekha.domain.interactors.impl.DeleteCartItemInteractorimpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.GetCartDetailsInteractorImpl;
import in.net.webinfotech.santirekha.domain.interactors.impl.UpdateCartInteractorImpl;
import in.net.webinfotech.santirekha.domain.model.Cart.Cart;
import in.net.webinfotech.santirekha.domain.model.Products.Product;
import in.net.webinfotech.santirekha.domain.model.User.UserInfo;
import in.net.webinfotech.santirekha.presentation.presenters.CartDetailsActivityPresenter;
import in.net.webinfotech.santirekha.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.santirekha.presentation.routers.CartIDetailsActivityRouter;
import in.net.webinfotech.santirekha.presentation.ui.adapters.CartItemListAdapter;
import in.net.webinfotech.santirekha.repository.ProductRepository.impl.CartRepositoryImpl;

/**
 * Created by Raj on 10-01-2019.
 */

public class CartDetailsActivityPresenterImpl extends AbstractPresenter implements CartDetailsActivityPresenter,
                                                                                    GetCartDetailsInteractor.Callback,
                                                                                    CartItemListAdapter.Callback,
                                                                                    DeleteCartItemInteractor.Callback,
                                                                                    UpdateCartInteractor.Callback{

    Context mContext;
    AndroidApplication androidApplication;
    GetCartDetailsInteractorImpl mCartDetailsInteractor;
    DeleteCartItemInteractorimpl mDeleteCartinteractor;
    int userId;
    CartDetailsActivityPresenter.View mView;
    CartItemListAdapter adapter;
    CartIDetailsActivityRouter mRouter;
    UserInfo userInfo;
    UpdateCartInteractorImpl updateCartInteractor;

    public CartDetailsActivityPresenterImpl(Executor executor,
                                            MainThread mainThread,
                                            Context context,
                                            CartDetailsActivityPresenter.View view,
                                            CartIDetailsActivityRouter router) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void getCartList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        mCartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                                                                    mMainThread,
                                                                    new CartRepositoryImpl(),
                                                                    this,
                                                                    userInfo.userId,
                                                                    userInfo.apiKey);
        mCartDetailsInteractor.execute();
    }

    @Override
    public void onGetCartDetailsSuccess(Cart[] carts) {
        int sum = 0;
        boolean status = true;
        for(int i = 0; i < carts.length; i++){
            sum = sum + carts[i].quantity * carts[i].price;
            if(carts[i].stock == 0 || carts[i].stock < carts[i].quantity){
                status = false;
            }
        }
        mView.loadCartSubtotal(sum, carts.length, status);
        adapter = new CartItemListAdapter(mContext, this, carts);
        mView.loadCartItemList(adapter, carts);
        mView.hideLoadingProgress();
    }

    @Override
    public void onGetCartDetailsFail(String errorMsg) {
        mView.hideLoadingProgress();
        Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPreviewItemClicked(int position) {
        mView.showUpdateDialog(position);
    }

    @Override
    public void updateCart(int cartId, int quantity) {
        updateCartInteractor = new UpdateCartInteractorImpl(mExecutor, mMainThread, new CartRepositoryImpl(), this, quantity, userInfo.userId, cartId, userInfo.apiKey);
        updateCartInteractor.execute();
    }

    @Override
    public void onUpdateCartSuccess(String successMsg) {
        Toast.makeText(mContext, successMsg, Toast.LENGTH_SHORT).show();
        getCartList();
    }

    @Override
    public void onUpdateCartFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteItemClicked(int cartId) {

        mView.showLoadingProgress();
        mDeleteCartinteractor = new DeleteCartItemInteractorimpl(mExecutor,
                                                                    mMainThread,
                                                                    new CartRepositoryImpl(),
                                                                    this,
                                                                    userInfo.userId, cartId, userInfo.apiKey);
        mDeleteCartinteractor.execute();
    }

    @Override
    public void onCartItemDeletedSuccess(String successMsg) {
        Toast.makeText(mContext,"Item successfully removed",Toast.LENGTH_SHORT).show();
        getCartList();
    }

    @Override
    public void onCartItemDeleteFail(String errorMsg) {
        Toast.makeText(mContext,errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
