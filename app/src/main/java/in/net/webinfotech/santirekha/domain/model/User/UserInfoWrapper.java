package in.net.webinfotech.santirekha.domain.model.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.net.webinfotech.santirekha.domain.model.Category.Category;

/**
 * Created by Raj on 12-02-2019.
 */

public class UserInfoWrapper {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public UserInfo userInfo;

}
