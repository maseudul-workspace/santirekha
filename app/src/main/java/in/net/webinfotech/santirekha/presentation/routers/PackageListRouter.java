package in.net.webinfotech.santirekha.presentation.routers;

/**
 * Created by Raj on 28-02-2019.
 */

public interface PackageListRouter {
    void goToProductDetailsActivity(int id);

}
